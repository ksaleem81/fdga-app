//
//  LoginViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/28/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import Crashlytics

class AcademyViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionLbl: UILabel!
    //@IBOutlet weak var descriptionLbl: UITextView!
    @IBOutlet weak var descriptionContainerView: UIView!
    @IBOutlet weak var desContainerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var studentBtn: UIButton!
    var acedmyId = ""
    var data :[[String:AnyObject]] = [[String:AnyObject]]()
    var needToReloadOrNot = true
    @IBOutlet weak var mainImg: UIImageView!
    @IBOutlet weak var imgHeightCons: NSLayoutConstraint!
    @IBOutlet weak var botomBtnsView: UIView!
    //MARK:- life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        descriptionLbl.contentMode = .center
        // Do any additional setup after loading the view.
        if needToReloadOrNot {
            refreshData()
        }else{
            fillInfoWithExhistingData()
        }
        

        
        customisingBGImage()
    }
    
    func customisingBGImage() {
        
        let bgImage = UIImageView()
            if currentTarget == "Chris Ryan Golf"{
                let frame = UIImage().returnImageFrame(image: UIImage(named:"chrisRyan_loginBg")!, view: self.view)
                adjustingBgImageFrame(imageName: "chrisRyan_loginBg", frame: frame, bgImage:bgImage, mode: .scaleAspectFill)
                
            }else if currentTarget == "Playgolf"{
               
                let frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 250)
                adjustingBgImageFrame(imageName: "logon_PG", frame: frame, bgImage:bgImage, mode: .scaleAspectFill)
            }else if currentTarget == "Jay Kelly Golf"{
                bgImage.frame =  CGRect(x: 15, y: 20, width:self.view.frame.size.width-30, height: 200)
                bgImage.image = UIImage(named:"JayKellyGolf-1")
                bgImage.contentMode = .scaleAspectFit
                self.containerView.addSubview(bgImage)
                mainImg.image = UIImage(named:"")
                bgImage.clipsToBounds = true

            }else if currentTarget == "RLIgolf"{
                
                var height = 270
                if Utility.isiPhonX(){
                height = height + 60
                }
                let frame = CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height:height )
               adjustingBgImageFrame(imageName: "academyBG", frame: frame, bgImage:bgImage, mode: .scaleAspectFill)

           }else if currentTarget == "3Hammers golf"{
                var height = 375
                if Utility.isiPhonX(){
                    height = height + 40
                }
            let frame = CGRect(x: 0, y: 0, width:Int(self.view.frame.size.width), height:height )
            adjustingBgImageFrame(imageName: "logon3Hammers", frame: frame, bgImage: bgImage, mode: .scaleAspectFill)
            }else if currentTarget == "TPC Group"{
                let height = 375
                let frame = CGRect(x: 0, y: 0, width:Int(self.view.frame.size.width), height:height )
                adjustingBgImageFrame(imageName: "academyBgTPC", frame: frame, bgImage: bgImage, mode: .scaleAspectFill)
            }else if currentTarget == "Thorpe Wood Golf"{
                let height = 375
                let frame = CGRect(x: 0, y: 0, width:Int(self.view.frame.size.width), height:height )
                adjustingBgImageFrame(imageName: "academyBgThorpwood", frame: frame, bgImage: bgImage, mode: .scaleAspectFill)
                
            }else if currentTarget == "Suzy Whaley Golf"{
                let height = 375
                let frame = CGRect(x: 0, y: 0, width:Int(self.view.frame.size.width), height:height )
                adjustingBgImageFrame(imageName: "suziacademyBG", frame: frame, bgImage: bgImage, mode: .scaleAspectFill)
            }
            else if currentTarget == "Belfry Golf"{
                var height = 270
                if Utility.isiPhonX(){
                    height = height + 60
                }

                let frame = CGRect(x: 0, y: 0, width:Int(self.view.frame.size.width), height:height )
                adjustingBgImageFrame(imageName: "belfryAcademyBG", frame: frame, bgImage: bgImage, mode: .scaleAspectFill)
        }

        
        
    }
    
    func adjustingBgImageFrame(imageName:String,frame:CGRect,bgImage:UIImageView,mode:UIViewContentMode)  {
        bgImage.frame = frame//
        bgImage.image = UIImage(named:imageName)
        bgImage.autoresizingMask = [.flexibleHeight, .flexibleBottomMargin ]
        bgImage.contentMode = mode // OR .scaleAspectFill
        bgImage.clipsToBounds = true
        self.mainImg.addSubview(bgImage)
        mainImg.image = UIImage(named:"")
        
    }
    
    //MARK:- data from server
    func refreshData()  {
        
        let refreshCont = RefreshingViewController()
        refreshCont.refreshData(acedmyId: acedmyId, onSuccess: { (data1) in
            
//            let data1 = data as! [[String:AnyObject]]
            self.data = data1 as! [[String:AnyObject]]
            print(self.data)
            self.settingDataToViews()


//            if data1.count > 0 {
//                let dic = data1[0]
//                print(dic)
//                DataManager.sharedInstance.updateAcademyDetails(dic as NSDictionary)
//
//            }
            
        }) { (error) in
            self.showInternetError(error: error!)
        }
    
    }

    func settingDataToViews()  {
        
        if data.count > 0 {
            let dic = data[0]
            print(dic)
            DataManager.sharedInstance.updateAcademyDetails(dic as NSDictionary)
            if let title = dic["AcademyName"] as? String {
                self.navigationItem.title = title
            }
            
            if currentTarget == "Chris Ryan Golf"{
                self.navigationItem.title = "Chris Ryan Golf"
            }
            
            if let title = dic["AcademyDescriptin"] as? String {
                var height = DataManager.sharedInstance.heightForView(text: title, font: UIFont.systemFont(ofSize: 12.0), width:view.frame.size.width - 20 )
                
                //just to cover design for iphonX background image can be omited
                if currentTarget == "3Hammers golf"{
                if Utility.isiPhonX(){
                      height = height + 40
                    }
                    
                }
                //till here
                self.desContainerHeightConstraint.constant = height  + 20
                self.containerViewHeightConstraint.constant = 400 + height  + 20
                self.descriptionLbl.lineBreakMode = .byWordWrapping
                self.descriptionLbl.text = title
                descriptionLbl.sizeToFit()
            }
        }
    }

    func fillInfoWithExhistingData()  {
        
            let dic = DataManager.sharedInstance.currentAcademy()
            if let title = dic?["AcademyName"] as? String {
                self.navigationItem.title = title
            }
    
        if currentTarget == "Chris Ryan Golf"{
            self.navigationItem.title = "Chris Ryan Golf"
        }
            if let title = dic?["AcademyDescriptin"] as? String {
                let height = DataManager.sharedInstance.heightForView(text: title, font: UIFont.systemFont(ofSize: 12.0), width:view.frame.size.width - 20 )
                self.desContainerHeightConstraint.constant = height  + 20
                self.containerViewHeightConstraint.constant = 400 + height  + 20
                self.descriptionLbl.lineBreakMode = .byWordWrapping
                self.descriptionLbl.text = title
                descriptionLbl.sizeToFit()
            }
        
    }
    
    func rectForText(text: String, font: UIFont, maxSize: CGSize) -> CGSize {
        
        let attrString = NSAttributedString.init(string: text, attributes: [NSAttributedStringKey.font:font])
        let rect = attrString.boundingRect(with: maxSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, context: nil)
        let size = CGSize(width:rect.size.width, height:rect.size.height)
        return size
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- buttons actions
    
    @IBAction func loginBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func newStudentBtnAction(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension UIImage{
    
    func returnImageFrame(image:UIImage,view:UIView) -> CGRect {
        let originalImage = image
        let width = originalImage.size.width;
        let height = originalImage.size.height;
        let apect = width/height;
        let nHeight = view.frame.size.width / apect
        return CGRect(x:0, y:0, width:view.frame.size.width, height:nHeight);
    }
    
}

//for playgolf

//                bgImage.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 250)
//                bgImage.image = UIImage(named:"logon_PG")
//                bgImage.autoresizingMask = [.flexibleHeight, .flexibleBottomMargin ]
//                bgImage.contentMode = .scaleAspectFill // OR .scaleAspectFill
//                bgImage.clipsToBounds = true
//                self.mainImg.addSubview(bgImage)
//                mainImg.image = UIImage(named:"")



