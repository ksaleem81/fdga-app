//
//  VideoPlayerViewController.h
//  sibme2.0
//
//  Created by MacBook Pro on 01/07/2015.
//
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import  "SAVideoRangeSlider.h"
@interface VideoPlayerViewController : UIViewController<UIActionSheetDelegate,MPMediaPickerControllerDelegate,UIScrollViewDelegate,UITableViewDelegate>
{
    
    NSURL *fileURL;
    IBOutlet UIView *sliderView;
    IBOutlet UIButton *trimBtn;
    NSString *newVideoPath;
    IBOutlet UILabel *titleLbl;
    IBOutlet UIActivityIndicatorView *activityView;
    IBOutlet UIView *vCView;
    int currentTimerVal;
    NSString *newLibraryPath;
    BOOL viewAdded;
    BOOL isSeeking;
    BOOL seekToZeroBeforePlay;
    float mRestoreAfterScrubbingRate;
    
    IBOutlet UIBarButtonItem *mPlayButton;
    IBOutlet UIBarButtonItem *mStopButton;
    AVPlayerItem * mPlayerItem;
    AVPlayerLayer *layer;
    IBOutlet UIScrollView *srcView;
    NSString *urlSt;
    int scrollViewSize;
    NSMutableArray *overlayA;
    IBOutlet UIButton *playPauseBtn;
    BOOL showHide;
    IBOutlet UIView *scrollBgView;
 //   IBOutlet UILabel *overlayLbl;
    float durationVal;
    CGFloat resumeVal;
    BOOL isPause;
    BOOL scrubbing;

}

@property (nonatomic, strong) id mTimeObserver;
@property BOOL isOnlineVideo;
//@property(nonatomic,retain)NSString *urlSt;
@property(nonatomic,strong) IBOutlet UIView *scrollBgView;
@property (strong) AVPlayerItem* mPlayerItem;
//@property(nonatomic,retain) IBOutlet  UIButton *  playBtn;
@property(nonatomic,retain) IBOutlet  UIButton *  doneBtn;
@property(nonatomic,retain)  NSDictionary *  videoInfo;
@property(nonatomic,retain)  SAVideoRangeSlider *  mySAVideoRangeSlider;
//@property(nonatomic,retain)   AVPlayer *   videoController;
@property(nonatomic,retain)NSString *videoPath;
@property (readwrite, strong, setter=setPlayer:, getter=player) AVPlayer* videoController;

-(IBAction)playPuaseBtnClicked:(id)sender;
-(IBAction)playVideo:(id)sender;
-(IBAction)doneBtn:(id)sender;
-(IBAction)selectBtn:(id)sender;
-(void)hidescrum;
@end
