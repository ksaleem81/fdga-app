//
//  UploadProgressCell.swift
//  GLF. Locker
//
//  Created by Jawad Ahmed on 15/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit

class UploadProgressCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var stopPlayButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
