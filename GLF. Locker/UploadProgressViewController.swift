//
//  UploadProgressViewController.swift
//  GLF. Locker
//
//  Created by Jawad Ahmed on 15/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit

class UploadProgressViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var noMediaLabel: UILabel!
    var dataSource: Array<Any> = []
    var cellsDictionary: Dictionary<String, UploadProgressCell> = [:]
    
    
    /************************************************************************************************************/
    // MARK: - LifeCycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "UPLOAD PROGRESS"
        GlobalClass.sharedInstance().setUploadProgressViewController(self)
        resetData()
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//
//        let controller = storyboard.instantiateViewController(withIdentifier: "SelectVideosViewController") as! SelectVideosViewController

//        if (self.navigationController?.viewControllers.contains(controller))!{
           self.navigationController?.isNavigationBarHidden = false
//
//        }
   
        table.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
         self.table.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        if (self.navigationController?.viewControllers.count)! < 2 {
        }else{
//            self.navigationController?.isNavigationBarHidden = true
        }

    }
    /************************************************************************************************************/
    // MARK: - Public Methods
    
    @objc func updateProgress(with info: Dictionary<String, Any>) {
        let mediaID = info["uniqueMediaID"] as! String
        
        let cell = cellsDictionary[mediaID]
        if let status =  info["isVideoUploaded"] as? String , status=="YES"{
            DispatchQueue.main.async {

                cell?.progressLabel.text = String.localizedStringWithFormat("Uploaded Completed")
                
            }
            return
        }else{
            
        }
        let uploadedMbs = (info["uploadedBytes"]! as! Double) / (1024*1024)
        let totalMbs = (info["totalBytes"] as! Double) / (1024*1024)
        
        DispatchQueue.main.async {
            cell?.progressLabel.text = String.localizedStringWithFormat("Uploaded %.2f MB of %.2f MB", uploadedMbs, totalMbs)
        }
    }
    
   @objc func resetData() {
        if let videos = UploadManager.shared().getRecentUploadsList() {
            dataSource = videos
            table.reloadData()
            
            if (videos.count > 0) {
                table.isHidden = false
                noMediaLabel.isHidden = true
            }
            else {
                table.isHidden = true
                noMediaLabel.isHidden = false
            }
        }
        else {
            table.isHidden = true
            noMediaLabel.isHidden = false
        }
    }
    
    /************************************************************************************************************/
    // MARK: - IBActions
    
    @IBAction func cellStopPlayAction(_ sender: UIButton) {
        let vidInfo = dataSource[sender.tag] as! Dictionary<String, Any>
        let statusKey = (vidInfo["uploadRequestStatusKey"] as! NSNumber).uintValue
        let dict = NSMutableDictionary.init(dictionary: vidInfo)
        
        if (statusKey == UploadRequestStatusSuspended.rawValue) {
            UploadManager.shared().resumeUploadingMedia(with: dict)
        }
        else {
            UploadManager.shared().stopUploadingMedia(with: dict)
        }
        resetData()
    }
    
    @IBAction func cellCancelAction(_ sender: UIButton) {
        let vidInfo = dataSource[sender.tag] as! Dictionary<String, Any>
        let dict = NSMutableDictionary.init(dictionary: vidInfo)
        UploadManager.shared().cancelUploadingMedia(with: dict)
        resetData()
    }
    
    /************************************************************************************************************/
    // MARK: - UItableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "UploadProgressCell") as? UploadProgressCell
        if (cell == nil) {
            cell = Bundle.main.loadNibNamed("UploadProgressCell", owner: self, options: nil)?.first as? UploadProgressCell
        }
        
        let vidInfo = dataSource[indexPath.row] as! Dictionary<String, Any>
        let mediaID = vidInfo["uniqueMediaID"] as! String
        
        if cellsDictionary[mediaID] == nil {
            cellsDictionary[mediaID] = cell
        }
        
        cell?.titleLabel.text = vidInfo["description"] as? String
        cell?.stopPlayButton.tag = indexPath.row
        cell?.stopPlayButton.addTarget(self, action: #selector(cellStopPlayAction(_:)), for: .touchUpInside)
        cell?.cancelButton.tag = indexPath.row
        cell?.cancelButton.addTarget(self, action: #selector(cellCancelAction(_:)), for: .touchUpInside)
        
        let statusKey = (vidInfo["uploadRequestStatusKey"] as! NSNumber).uintValue
        if (statusKey != UploadRequestStatusUploaded.rawValue) {
            if (statusKey != UploadRequestStatusNotStarted.rawValue) {
                if (statusKey == UploadRequestStatusSuspended.rawValue) {
                    cell?.stopPlayButton.setImage(UIImage.init(named: "playIcon"), for: .normal)
                }
                else {
                    cell?.stopPlayButton.setImage(UIImage.init(named: "stopIcon"), for: .normal)
                }
            }
        }
        
        return cell!
    }
}
