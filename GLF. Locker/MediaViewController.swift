//
//  MediaViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/20/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu
class MediaViewController: UIViewController {

    @IBOutlet weak var discriptionLbl: UILabel!
    @IBOutlet weak var containerViewHeightConst: NSLayoutConstraint!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var label5: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if currentUserLogin == 4 {
            self.label5.text = "NOTES"
        }else{
        }
//        self.settingRighMenuBtn()
//        MBProgressHUD.showAdded(to:self.appDelegate.window?.topMostController()?.view!, animated: true)
        

    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.isEnabled = false
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(MediaViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        DispatchQueue.main.asyncAfter(deadline: .now() +  1.0) { // change 2 to desired number of seconds
//
//
//        }
//
//    }
    
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        self.navigationController?.isNavigationBarHidden = false
//        
//    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Buttons Actions

    @IBAction func button1Action(_ sender: UIButton) {
        
        let actionSheetController: UIAlertController = UIAlertController(title:"SELECT MEDIA", message:nil, preferredStyle: .actionSheet)
        let logOutAction: UIAlertAction = UIAlertAction(title: "CLOSE", style: .destructive) { action -> Void in
            //Do some stuff
        }
        let photoAction: UIAlertAction = UIAlertAction(title: "PHOTO", style: .default) { action -> Void in
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "SelectMediaViewController") as! SelectMediaViewController
            controller.mediaType = kMediaTypePhoto
            self.navigationController?.pushViewController(controller, animated: true)
        }
        let videoAction: UIAlertAction = UIAlertAction(title: "VIDEO", style: .default) { action -> Void in
            
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "SelectMediaViewController") as! SelectMediaViewController
//            controller.previousData = previousData
//            controller.selectedData = selectedData
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        let drilAction: UIAlertAction = UIAlertAction(title: "DRILL", style: .default) { action -> Void in
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "SelectMediaViewController") as! SelectMediaViewController
            controller.mediaType = kMediaTypeDrill
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
        actionSheetController.addAction(photoAction)
        actionSheetController.addAction(videoAction)
        actionSheetController.addAction(drilAction)
        actionSheetController.addAction(logOutAction)
        if let popoverPresentationController = actionSheetController.popoverPresentationController {
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = CGRect(x:self.view.bounds.size.width, y: 0,width:1.0, height:1.0)
        }
        
        self.present(actionSheetController, animated: true, completion: nil)
    }

    @IBAction func button2Action(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VideoWebViewController") as! VideoWebViewController
        controller.mediaType = "2"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func button3Action(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VideoWebViewController") as! VideoWebViewController
        controller.mediaType = "1"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func button4Action(_ sender: UIButton) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VideoWebViewController") as! VideoWebViewController
        controller.mediaType = "3"
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func button5Action(_ sender: UIButton) {

        if currentUserLogin == 4 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "StudentNotesViewController") as! StudentNotesViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "VideoWebViewController") as! VideoWebViewController
        controller.mediaType = "1"
        controller.fromPlayerRecended = true
        self.navigationController?.pushViewController(controller, animated: true)
        }
    }

}
