//
//  OrderConfirmationViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/11/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu
class OrderConfirmationViewController: UIViewController {

    @IBOutlet weak var confirmationLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var discountLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    var forClassBooking = false
   var discountInfoGotOrNot = false
       var titleOfClass = ""
    var detailData = [String:AnyObject]()
    var totalDiscount = 0.0
    var responceString = ""
    var emailId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        print(detailData)
        // Do any additional setup after loading the view.
        settingInfoToViews()
        settingRighMenuBtn()
      
       let newBackButton = UIBarButtonItem(image: UIImage(named: "header_arrow"), landscapeImagePhone: UIImage(named: "header_arrow"), style: .plain, target: self, action: #selector(OrderConfirmationViewController.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    func settingInfoToViews()  {
        //for class confirmation code 01~9687~25.00~0.00#
        emailLbl.text = emailId
        let dateOrignal = responceString.components(separatedBy: "~")
        if dateOrignal.count > 2 {

           // let spaceCount = responceString.characters.filter{$0 == "#"}.count
            
            
            if forClassBooking {
                settingPrices()
//                confirmationLbl.text = "\(dateOrignal[0])"
//                if discountInfoGotOrNot{
//                     priceLbl.text = "Price:£ \(dateOrignal[2])" + "," + "Discount:\(dateOrignal[3])"
//                }else{
//                    priceLbl.text = "Price:£ \(dateOrignal[2])"
//                }
//                if let program = detailData["ProgramTypeName"] as? String{
//                    discountLbl.text = "Classes Booked: " +  titleOfClass //program
//                }
                
            }else{
                //38297~30.00~0.00
                if discountInfoGotOrNot{
                         discountLbl.text = "Discount:\(dateOrignal[2])"
                }else{
                    discountLbl.isHidden = true
                }
           
                confirmationLbl.text = "\(dateOrignal[0])"
                priceLbl.text = "Price:\(currencySign) \(dateOrignal[1])"
            }
            
        }

    }
    
    func settingPrices()  {
        
        
        let forOrder = responceString.components(separatedBy: "~")
        let oneString = responceString.components(separatedBy: "#")
        let spaceCount = responceString.characters.filter{$0 == "#"}.count
        
        var totalAmount = 0.0
        for i in 0...spaceCount-1 {
            let original = oneString[i].components(separatedBy: "~")
            
            totalAmount = totalAmount +  Double(original[2])!
        }
        
        confirmationLbl.text = "\(forOrder[0])"
        if discountInfoGotOrNot{
            priceLbl.text = "Price:\(currencySign) \(totalAmount)" + "," + "Discount:\(totalDiscount)"
        }else{
            priceLbl.text = "Price:\(currencySign) \(totalAmount)"
        }

        discountLbl.text = "Classes Booked: " +  titleOfClass //program
      

    }
    
    @objc func back(sender: UIBarButtonItem) {
       
        _ = self.navigationController?.popToRootViewController(animated: true)
    }

    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(OrderConfirmationViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) 
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeBtnAction(_ sender: UIButton) {
      _ = self.navigationController?.popToRootViewController(animated: true)
    }


}
