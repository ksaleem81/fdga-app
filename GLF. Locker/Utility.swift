//
//  Utility.swift
//  GLF. Locker
//
//  Created by Jawad Ahmed on 09/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import AVKit
import ActionSheetPicker_3_0
import Alamofire

class Utility: NSObject {
    
    static var userPassword = ""
    static var GDPRstatusDic = [String:AnyObject]()
    
    static func playVideo(path: String, on viewController: UIViewController) {
        let url = URL.init(fileURLWithPath: path)
        let avplayer = AVPlayer.init(url: url)
        let avplayerVC = AVPlayerViewController.init()
        avplayerVC.player = avplayer
        
        viewController.present(avplayerVC, animated: true) { 
            avplayerVC.player?.play()
        }
    }
    
    @objc static func showAlertView(title: String?, message: String?) {
        
        let alertController = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(action)
        
        let alertWindow = UIWindow.init()
        alertWindow.rootViewController = UIViewController.init()
        
        let topWindow = UIApplication.shared.windows.last
        alertWindow.windowLevel = (topWindow?.windowLevel)! + 1
        
        alertWindow.makeKeyAndVisible()
        alertWindow.backgroundColor = UIColor.clear
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    static func createCallBackID() -> String {
        return "VideoHandlerPlugin\(NSDate().timeIntervalSince1970)"
    }
    
    
    
    
    /************************************************************************************************************/
    // MARK: - Analyze and Trim Methods
    
    static func showShapeViewController(with videoInfo: [AnyHashable : Any], presenter: UIViewController, animated: Bool, delegate: ShapeViewControllerDelegate?) {
        
        let shapeViewController = ShapeViewController.init(videoInfo: videoInfo)
        shapeViewController?.delegate = delegate
        if (shapeViewController?.responds(to: #selector(ShapeViewController.reloadView)))! {
            shapeViewController?.reloadView()
        }
        let navigationController = UINavigationController.init(rootViewController: shapeViewController!)
        presenter.present(navigationController, animated: animated, completion: nil)
        
    }
    
    static func showVideoPlayerController(with videoInfo: [AnyHashable : Any], presenter: UIViewController, animated: Bool, isOnline: Bool) {
        
        let vPlayObj = VideoPlayerViewController.init(nibName: "VideoPlayerViewController", bundle: nil)
        vPlayObj.videoInfo = videoInfo;
        vPlayObj.isOnlineVideo = isOnline;
        
        let navigationController = UINavigationController.init(rootViewController: vPlayObj)
        navigationController.restorationIdentifier = "world"
        presenter.present(navigationController, animated: animated, completion: nil)
        
    }
    
    //MARK:- animation method
    static func viewToShake(viewToShake:UIView)  {
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: viewToShake.center.x - 10, y: viewToShake.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: viewToShake.center.x + 10, y: viewToShake.center.y))
        viewToShake.layer.add(animation, forKey: "position")
        
    }
    
    //MARK:- device detection method

     static func isiPhonX() -> Bool {
        if UIDevice().userInterfaceIdiom == .phone {
            let height =  UIScreen.main.nativeBounds.height
            if height == 2436{
                return true
            }
        }
        return false
    }
    
    //MARK:- picker time retriction

     static func settingTimeLimitToPicker(datePicker:ActionSheetDatePicker) -> ActionSheetDatePicker {
        let startHour: Int = 7
        let endHour: Int = 22
        let date1: NSDate = NSDate()
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let components: NSDateComponents = gregorian.components(([.day, .month, .year]), from: date1 as Date) as NSDateComponents
        components.hour = startHour
        components.minute = 0
        components.second = 0
        let startDate: NSDate = gregorian.date(from: components as DateComponents)! as NSDate
        components.hour = endHour
        components.minute = 0
        components.second = 0
        let endDate: NSDate = gregorian.date(from: components as DateComponents)! as NSDate
        datePicker.minimumDate = startDate as Date?
        datePicker.maximumDate = endDate as Date?
        return datePicker
    }

    //MARK:- generic method for password field Strength

   static func passwordStrength(passwordString:String,messageString1:String)->String  {
    
        print(passwordString)
        var messageString = messageString1
        
    if (passwordString.count) < 8 {
        
        messageString = messageString + "Password Should be 8 characters with at least 1 letter, 1 number and 1 symbol.\n"
    }else{

        let regex = try! NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
        if regex.firstMatch(in: passwordString, options: NSRegularExpression.MatchingOptions(), range:NSMakeRange(0, passwordString.count)) != nil {
        }else{
            messageString = messageString + "Password should contain atleast 1 symbol.\n"
        }

        let numbersRange2 = passwordString.rangeOfCharacter(from: .decimalDigits)
        let hasNumbers2 = (numbersRange2 != nil)
    
        if hasNumbers2{
        }else{
            messageString = messageString + "Password should contain atleast 1 number.\n"
        }
        
        let numbersRange3 = passwordString.rangeOfCharacter(from: .letters)
        let hasNumbers3 = (numbersRange3 != nil)
        if hasNumbers3{
        }else{
            messageString = messageString + "Password should contain atleast 1 letter.\n"
        }
        
    }
    
    return messageString
    
    }

    

    static func getAthenticatedHeader(method:String) -> HTTPHeaders {
        
        var headers   = HTTPHeaders()
        
        let methodsArray = ["GetAllAcademiesOrderByLatLong","GetAllOwnerAcademiesOrderByLatLong","login","ResetPassword","SearchAcademyByID_native","BookingLesson","BookingCartClass","RegisterStudent_native"]
        
        if methodsArray.contains(method.components(separatedBy: "/")[1]){
        
            headers = [
                "Content-Type": "application/x-www-form-urlencoded"]
        }else{
            
            var token = ""
            
            if let id = DataManager.sharedInstance.currentUser()!.value(forKey: "TokenKey") as? String {
                token = "\(id)"
                print(token)
            }
            
            headers = [
                "Content-Type": "application/x-www-form-urlencoded",
                "UserName":"\(String(describing: DataManager.sharedInstance.currentUser()!.value(forKey: "UserName")!))",
                "AcademyId":"\(String(describing: DataManager.sharedInstance.currentUser()!.value(forKey: "AcademyId")!))",
                "AuthenticationToken":"\(token)",
                "UserID":"\(String(describing: DataManager.sharedInstance.currentUser()!.value(forKey: "Userid")!))"
            ]
        }
        
        return headers
    }

    
    //MARK:- authorize action
    static func authorizeAlert(){
        let dele = UIApplication.shared.delegate as! AppDelegate
        dele.athorizeError()
        
    }

}

//MARK:- loading customized View

/* var transparentView:UIView?
 var activityIndicatorBgV:UIView!
 var titleLbl:UILabel!
 var activityIndicatorCenterImage:UIImageView!
 var activityIndicatorImage:UIImageView!
 var activityIndicatorFlipImage:UIImageView!
 
 
 
 func rotateLayerInfinite(_ title:NSString) {
 
 DispatchQueue.main.async { () -> Void in
 if (self.transparentView == nil) {
 self.transparentView = UIView()
 }
 self.transparentView!.frame = self.window!.frame
 
 if (self.activityIndicatorBgV == nil) {
 self.activityIndicatorBgV = UIView()
 }
 self.activityIndicatorBgV.frame = CGRect(x: 0, y: 0, width: 110, height: 90)
 
 if (self.titleLbl == nil) {
 self.titleLbl = UILabel()
 }
 
 self.titleLbl.frame = CGRect(x: 5, y: 65, width: 100, height: 20)
 
 self.transparentView!.alpha = 0.4;
 self.transparentView!.backgroundColor =  UIColor.black
 self.window?.addSubview(self.transparentView!)
 
 let rotation:CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation")// [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
 rotation.fromValue = NSNumber(value: 0 as Float)
 rotation.toValue = NSNumber(value: 2 * M_PI as Double)
 rotation.duration = 1.0; // Speed
 rotation.repeatCount = .infinity
 
 let flipRotation:CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation")// [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
 flipRotation.fromValue = NSNumber(value: 0 as Float)
 flipRotation.toValue = NSNumber(value: -2 * M_PI as Double)
 flipRotation.duration = 1.0; // Speed
 flipRotation.repeatCount = .infinity
 //    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
 
 self.activityIndicatorBgV.center = CGPoint(x: self.transparentView!.center.x, y: self.transparentView!.center.y-25)
 self.activityIndicatorBgV.backgroundColor = ColorCodes.activityIndicatorBgColor
 self.activityIndicatorBgV.layer.cornerRadius = 10
 self.window?.addSubview(self.activityIndicatorBgV)
 
 self.titleLbl.font = UIFont(name: FontName.lightFont, size: 15)
 self.titleLbl.textColor = UIColor.black
 self.titleLbl.textAlignment = NSTextAlignment.center
 self.titleLbl.text = title as String
 self.activityIndicatorBgV.addSubview(self.titleLbl)
 
 if (self.activityIndicatorFlipImage == nil) {
 self.activityIndicatorFlipImage = UIImageView()
 }
 
 self.activityIndicatorFlipImage.frame = CGRect(x: 0, y: 0, width: 72, height: 72)
 self.activityIndicatorFlipImage.backgroundColor = UIColor.clear
 self.activityIndicatorFlipImage.center = CGPoint(x: self.activityIndicatorBgV.center.x, y: self.activityIndicatorBgV.center.y-10)
 self.activityIndicatorFlipImage.image = UIImage(named: "outer")
 //            activityIndicatorFlipImage.layer.removeAllAnimations()
 self.activityIndicatorFlipImage.layer.add(flipRotation, forKey: "Spin")
 self.window?.addSubview(self.activityIndicatorFlipImage)
 
 if (self.activityIndicatorImage == nil) {
 self.activityIndicatorImage = UIImageView()
 }
 
 self.activityIndicatorImage.frame = CGRect(x: 0, y: 0, width: 55, height: 55)
 self.activityIndicatorImage.backgroundColor = UIColor.clear
 self.activityIndicatorImage.center = CGPoint(x: self.activityIndicatorBgV.center.x, y: self.activityIndicatorBgV.center.y-10)
 self.activityIndicatorImage.image = UIImage(named: "inner")
 //           activityIndicatorImage.layer.removeAllAnimations()
 self.activityIndicatorImage.layer.add(rotation, forKey: "Spin")
 self.window?.addSubview(self.activityIndicatorImage)
 
 if (self.activityIndicatorCenterImage == nil) {
 self.activityIndicatorCenterImage = UIImageView()
 }
 self.activityIndicatorCenterImage.backgroundColor = UIColor.clear
 self.activityIndicatorCenterImage.frame = CGRect(x: 0, y: 0, width: 75, height: 75)
 self.activityIndicatorCenterImage.center = CGPoint(x: self.activityIndicatorBgV.center.x, y: self.activityIndicatorBgV.center.y-10)
 self.activityIndicatorCenterImage.image = UIImage(named: "logo")
 self.window?.addSubview(self.activityIndicatorCenterImage)
 }
 /*
 SVProgressHUD.setCornerRadius(0.0)
 SVProgressHUD.setFont(UIFont(name: FontName.regularFont, size: 14))
 SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.Gradient)
 SVProgressHUD.showWithStatus(title as String)
 
 */
 
 }
 
 func stopRotateLayer()  {
 //    SVProgressHUD.dismiss()
 
 if transparentView != nil{
 UIView.animate(withDuration: 0.5, animations: { () -> Void in
 self.titleLbl.alpha = 0.0
 self.activityIndicatorCenterImage.alpha = 0.0
 self.activityIndicatorImage.alpha = 0.0
 self.activityIndicatorFlipImage.alpha = 0.0
 self.transparentView!.alpha = 0.0
 }, completion: { (success) -> Void in
 if success{
 
 }
 })
 UIView.animate(withDuration: 0.5, animations: { () -> Void in
 self.activityIndicatorImage.layer.removeAllAnimations()
 self.activityIndicatorCenterImage.layer.removeAllAnimations()
 self.activityIndicatorFlipImage.layer.removeAllAnimations()
 
 self.titleLbl.removeFromSuperview()
 self.activityIndicatorCenterImage.removeFromSuperview()
 self.activityIndicatorImage.removeFromSuperview()
 self.activityIndicatorFlipImage.removeFromSuperview()
 self.activityIndicatorBgV.removeFromSuperview()
 self.transparentView!.removeFromSuperview()
 
 self.titleLbl = nil
 self.activityIndicatorCenterImage = nil
 self.activityIndicatorImage = nil
 self.activityIndicatorFlipImage = nil
 self.activityIndicatorBgV = nil
 self.transparentView = nil
 })
 }
 
 }
 
 struct FontName {
 static let thinFont = "TitilliumWeb-Thin"
 static let semiBoldFont = "TitilliumWeb-SemiBold"
 static let boldFont = "TitilliumWeb-Bold"
 static let lightFont = "TitilliumWeb-Light"
 static let regularFont = "TitilliumWeb-Regular"
 }
 
 struct ColorCodes {
 
 static let lightGrayColor = UIColor.lightGray
 static let headerBarColor = UIColor(red: 255.0/255.0, green: 102.0/255.0, blue: 3.0/255.0, alpha: 1.0)
 
 static let buttonTextColor = UIColor.black
 static let buttonTextHighlightColor = UIColor(red: 255.0/255.0, green: 102.0/255.0, blue: 3.0/255.0, alpha: 1.0)
 static let activityIndicatorBgColor = UIColor.white
 
 static let alertBgColor = UIColor.white
 static let menuBgView = UIColor(red: 62.0/255.0, green: 57.0/255.0, blue: 54.0/255.0, alpha: 1.0)
 }
 */

//        if method.components(separatedBy: "/")[1] == "GetAllAcademiesOrderByLatLong" || method.components(separatedBy: "/")[1] == "GetAllOwnerAcademiesOrderByLatLong" || method.components(separatedBy: "/")[1] == "login" || method.components(separatedBy: "/")[1] == "ResetPassword" || method.components(separatedBy: "/")[1] == "SearchAcademyByID_native" || method.components(separatedBy: "/")[1] == "BookingLesson" || method.components(separatedBy: "/")[1] == "BookingCartClass" || method.components(separatedBy: "/")[1] ==  "RegisterStudent_native"  {
