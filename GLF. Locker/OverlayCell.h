//
//  OverlayCell.h
//  Orbis
//
//  Created by Nasir Mehmood on 08/01/2014.
//
//

#import <UIKit/UIKit.h>

@interface OverlayCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UIImageView *overlayImageView;

@end
