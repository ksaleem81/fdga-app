//
//  GLFCalendarTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/27/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit

class GLFCalendarTableViewCell: UITableViewCell {
    @IBOutlet weak var programTitle: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
