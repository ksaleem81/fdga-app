//
//  MapTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit

class DateTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var companyLbl: UILabel!
 
    @IBOutlet weak var classTitleLbl: UILabel!
    @IBOutlet weak var numberAvailblBtn: UIButton!
   
    @IBOutlet weak var checkBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
