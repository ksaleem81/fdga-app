//
//  OrbisVideoLibrary.h
//  TestingShapes
//
//  Created by Nasir Mehmood on 07/01/2014.
//  Copyright (c) 2014 Nasir Mehmood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class OrbisMediaGallery;
@protocol OrbisMediaGalleryDelegate <NSObject>

@optional
- (void) orbisMediaGallery:(OrbisMediaGallery*)videoGallery preparedVideoAtPath:(NSString*)originalPath forUploadAtPath:(NSString*)newPath;
- (void) orbisMediaGallery:(OrbisMediaGallery*)videoGallery failedToPrepareVideoForUploadingAtPath:(NSString*)originalPath;


@end


@interface OrbisMediaGallery : NSObject

@property (nonatomic, weak) id<OrbisMediaGalleryDelegate> delegate;

@property (nonatomic) BOOL isCompressingVideo;

+ (OrbisMediaGallery*) sharedGallery;
+ (OrbisMediaGallery*) sharedGalleryWithDelegate:(id<OrbisMediaGalleryDelegate>) d;

- (NSArray*) getAllVideos;
- (NSArray*) getAllVideoDictionaries;

- (NSDictionary*) saveVideoToGalleryAtPath:(NSString*) videoPath;
- (NSString*) prepareVideoForUploadingAtPath:(NSString*) filePath;

- (NSDictionary*) saveVideoToGalleryWithPath:(NSString*) videoPath;
- (NSString*) getPathForNextLibraryVideo;

- (NSString*) moveVideoToMyLibraryAtPath:(NSString*)filePath;



- (NSString*) generateThumbnailForVideoAtPath:(NSString*) videoPath;

- (BOOL) deleteVideoFromGalleryAtPath:(NSString*) videoPath;

- (NSString*) getMediaGalleryFolderPath;
- (NSString*) getUploadMediaGalleryFolderPath;

- (NSDictionary*) saveOverlay:(UIImage*)overlayImage forVideo:(NSDictionary*)videoInfo;
- (NSArray*) getOverlaysForVideoAtPath:(NSString*)videoPath;

- (NSDictionary*) saveAnalysisImage:(CGImageRef)analysisImage atTime:(float)time forVideoAtPath:(NSString*)videoPath;
- (NSDictionary*) saveAnalysisImages:(NSArray*)analysisImages forVideo:(NSDictionary*)videoInfo;
- (NSDictionary*) saveAnalysisImages:(NSArray*)analysisImages forVideoAtPath:(NSString*)videoPath;


- (NSArray*) getAllPhotoDictionaries;
- (NSDictionary*) savePhotoToGalleryAtPath:(NSString*) photoPath;
- (BOOL) deletePhotoFromGalleryAtPath:(NSString*) photoPath;


- (UIImage*) rotateImage:(UIImage*)srcImage byDegrees:(CGFloat)degrees;
- (UIImage*) rotateImage:(UIImage*) src toOrientation:(UIImageOrientation)orientation;

- (void) CGImageWriteToFile:(CGImageRef)image atPath:(NSString*)path type:(CFStringRef)imageType;


@end
