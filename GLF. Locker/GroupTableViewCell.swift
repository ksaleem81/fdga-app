//
//  MapTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit

class GroupTableViewCell: UITableViewCell {

    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var studentImage: UIImageView!
    @IBOutlet weak var callImage: UIImageView!
 
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var callBtn: UIButton!
    @IBOutlet weak var arrowImg: UIImageView!
    @IBOutlet weak var deletBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        studentImage.layer.borderWidth = 0.25
        studentImage.layer.borderColor = UIColor.black.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
