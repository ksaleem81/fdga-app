//
//  DashBoardViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/29/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import SideMenu
import EventKit
import EventKitUI
import Alamofire
import Crashlytics
class DashBoardViewController: UIViewController, OrbisCameraViewControllerDelegate, GalleryViewControllerDelegate,ShapeViewControllerDelegate {
    var fromSideMenu = false
  
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerBtnHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgImageHeigthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button3: UIButton!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var button4: UIButton!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var label5: UILabel!
    @IBOutlet weak var button6: UIButton!
    @IBOutlet weak var label6: UILabel!
    @IBOutlet weak var button7: UIButton!
    @IBOutlet weak var label7: UILabel!
    @IBOutlet weak var button8: UIButton!
    @IBOutlet weak var label8: UILabel!
    @IBOutlet weak var label10: UILabel!
    @IBOutlet weak var button10: UIButton!
    var calendar: EKCalendar!
    var eventsArray = [[String:AnyObject]]()
    @IBOutlet weak var label9: UILabel!
    @IBOutlet weak var button9: UIButton!
    var coachArray = ["BOOK A LESSON","STUDENTS","SCHEDULE","UPCOMING LESSONS","MEDIA","VIEW ALL BOOKINGS","APP GALLERY","RECORD VIDEO","MESSAGES","STORE",nil]
    var studentArray = ["BOOK A LESSON","LESSON","MEDIA","MY PROFILE","MY GAME","MESSAGES","STORE",nil]
    let blurView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.light))
    override func viewDidLoad() {
        super.viewDidLoad()
        
      fetchingAndParsingData()
        //forcepassword
//       if let update = DataManager.sharedInstance.currentUser()!["IsPasswordReset"] as? Bool,update == true {
//        
//            self.appDelegate.addBlurView()
//        }
        
    }
    
    var messageString = ""
    
    func fetchingAndParsingData(){
        
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            currentCoachIdCoachLogin = "\(id)"
        }
        
        if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
            currentAcademyId = "\(id)"
        }
        
        var titleAcademy = ""
        if let titl =  DataManager.sharedInstance.currentAcademy()!["AcademyTitle"] as? String{
            titleAcademy = titl
        }
        
        if let titl =  DataManager.sharedInstance.currentAcademy()!["Currency"] as? String{
            currencySign =  titl
        }
        
        if let regionId  =  DataManager.sharedInstance.currentAcademy()!["RegionID"] as? Int{
            if regionId == 1 {
                globalDateFormate = "dd/MM/yyyy"
            }else if regionId == 2{
                globalDateFormate = "MM/dd/yyyy"
            }else{
                globalDateFormate = "dd/MM/yyyy"
            }
        }
        
        if let titl =  DataManager.sharedInstance.currentAcademy()!["OnlinePaymentType"] as? Int{
            if titl == 0{
                currentAcademyOnlinePayment = 0
            }else if titl == 1{
                currentAcademyOnlinePayment = 1
            }
        }
        
        storeLink = "https://\(titleAcademy).glflocker.com/Store/?AcademyId="
        self.navigationController?.isNavigationBarHidden = false
        settingDataToReletiveUser()
        var tableIdentifier = ""
        
        if currentUserLogin == 4 {
            tableIdentifier = "SideMenuTableViewController"
        }else{
            tableIdentifier = "SideMenuForCoachTableViewController"
            if fromSideMenu {
                
            }else{
                
                if self.appDelegate.keepMeLoginOn{
                }else{
                    self.appDelegate.keepMeLoginOn = true
                    getBookedCoachSchedule()
                }
            }
        }
        
        let conroler = self.storyboard?.instantiateViewController(withIdentifier: tableIdentifier)
        let menuRightNavigationController = UISideMenuNavigationController(rootViewController:conroler!)
        SideMenuManager.menuRightNavigationController = menuRightNavigationController
        SideMenuManager.menuFadeStatusBar = false
        registerDevice()
        NotificationCenter.default.addObserver(self, selector: #selector(recordVideo), name: NSNotification.Name(rawValue: "RecordVideo"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showVideoGallery), name: NSNotification.Name(rawValue: "ShowVideoGallery"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(importFromDevice), name: NSNotification.Name(rawValue: "ImportFromDevice"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showImageGallery), name: NSNotification.Name(rawValue: "ShowImageGallery"), object: nil)
        //settingLeftBarButton()
        if currentTarget == "GLF. Locker" {
        }else{
            self.title = currentTarget
        }
        
        //deciding image base url
        if baseUrl == "http://app.glfbeta.com/OrbisWebApi/api/" {
            imageBaseUrl = "http://\(titleAcademy).glfbeta.com"
        }else{
            imageBaseUrl = "https://\(titleAcademy).glflocker.com"
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

//        print(DataManager.sharedInstance.currentUser())
        //        GlobalClass.sharedInstance().shouldAllowLandscape = false
        
    }
    
    func settingButtonsBackGrounColors() {
        
        button1.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.50)
        button2.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.30)
        button3.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.30)
        button4.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.50)
        button5.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.50)
        button6.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.30)
        button7.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.30)
        button8.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.50)
        button9.backgroundColor = UIColor.init(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.50)
    }
    
    func settingLeftBarButton()  {
        
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "notification"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(DashBoardViewController.leftBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
    }
    
    @objc func leftBarBtnAction() {
        
        let storyboard = UIStoryboard(name: "Messages", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func registerDevice()  {
        
        var coachId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            coachId = "\(id)"
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate

        var userID = ""
        if let userId =   DataManager.sharedInstance.currentUser()!["Userid"]! as? Int{
            userID = "\(userId)"
        }
        
        let appVersion: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        var parameters : [String:String] = [String:String]()
        parameters = ["CoachId":coachId ,"deviceId":UIDevice.current.identifierForVendor!.uuidString,"deviceToken":appDelegate.deviceTokenInApp,"deviceType":UIDevice.current.modelName ,"UserId":userID ,"DeviceName":UIDevice.current.name ,"OSVersion":UIDevice.current.systemVersion ,"AppVersion":appVersion as! String]
        print(parameters)
        NetworkManager.performRequest(type:.post, method: "Academy/RegisterDevice/", parameter:parameters as [String : AnyObject]?, view: (UIApplication.getTopestViewController()!.view), onSuccess: { (object) in
            print(object!)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
            default:
                break
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
    }
    
    func settingDataToReletiveUser() {
        
        if let userType = DataManager.sharedInstance.currentUser()?["UserRoleId"] as? Int{
            currentUserLogin = userType
            if currentUserLogin == 4 {
                settingBGImageForPlayer()
                assigninImagesAndTitles(array: studentArray as NSArray)
                hiddingComponentsForReuseAbility()
                settingBtnsImagesForStudent()//used to changes button images for student
            }else{
                settingBGImageForCoach()
                assigninImagesAndTitles(array: coachArray as NSArray)
                showingComponentsForReuseAbility()
                settingBtnsImagesForCoach()//used to change button images for coach
            }
        }
    }
    
    func settingBtnsImagesForStudent() {
        
        button1.setImage(UIImage(named: "book_lesson"), for: .normal)
        button2.setImage(UIImage(named: "lesson"), for: .normal)
        button3.setImage(UIImage(named: "photo_media"), for: .normal)
        button4.setImage(UIImage(named: "student_1"), for: .normal)
        button5.setImage(UIImage(named: "student_1"), for: .normal)
        button6.setImage(UIImage(named: "message_icon"), for: .normal)
        button7.setImage(UIImage(named: "icon_83"), for: .normal)
        button8.setImage(UIImage(named: ""), for: .normal)
    }
    
    func settingBtnsImagesForCoach() {
        
        button1.setImage(UIImage(named: "book_lesson"), for: .normal)
        button2.setImage(UIImage(named: "student_1"), for: .normal)
        button3.setImage(UIImage(named: "schedule_icon"), for: .normal)
        button4.setImage(UIImage(named: "book_lesson"), for: .normal)
        button5.setImage(UIImage(named: "photo_media"), for: .normal)
        button6.setImage(UIImage(named: "most_view"), for: .normal)
        button7.setImage(UIImage(named: "media_upload"), for: .normal)
        button8.setImage(UIImage(named: "media_upload"), for: .normal)
        button9.setImage(UIImage(named: "message_icon"), for: .normal)
        button10.setImage(UIImage(named: "icon_83"), for: .normal)
    }
    
    func showingComponentsForReuseAbility() {
        
        button6.isHidden = false
        button7.isHidden = false
        button8.isHidden = false
        label6.isHidden = false
        label7.isHidden = false
        label8.isHidden = false
        
    }
    
    func hiddingComponentsForReuseAbility()  {
       // button6.isHidden = true
       // button6.setImage(UIImage(), for: .normal)
        //button7.isHidden = true
      //  button8.isHidden = true
//        label6.isHidden = true
       // label7.isHidden = true
        label8.isHidden = true
    }
    
    func assigninImagesAndTitles(array:NSArray)  {
        
        for index in 0...array.count - 1 {
            if index == 0 {
                label1.text = array[index] as? String
            }else if index == 1{
                label2.text = array[index] as? String
            }else if index == 2{
                label3.text = array[index] as? String
            }else if index == 3{
                label4.text = array[index] as? String
            }else if index == 4{
                label5.text = array[index] as? String
            }else if index == 5{
                label6.text = array[index] as? String
            }else if index == 6{
                label7.text = array[index] as? String
            }else if index == 7{
                label8.text = array[index] as? String
            }else if index == 8{
                label9.text = array[index] as? String
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- UIbuttons actions
    
    @IBAction func button1Action(_ sender: UIButton) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "BookLessonVC") as! BookLessonVC
        self.navigationController?.pushViewController(controller, animated: true)
        
    }

    @IBAction func button2Action(_ sender: UIButton) {
        
         if currentUserLogin == 4 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "LessonsViewController") as! LessonsViewController
            self.navigationController?.pushViewController(controller, animated: true)
            }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "CustomeTabViewController") as! CustomeTabViewController
            controller.viewAppearedOrNot = true
            self.navigationController?.pushViewController(controller, animated: true)
            }
    }
    
    @IBAction func button3Action(_ sender: UIButton) {
        
        if currentUserLogin == 4  {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleViewController") as! ScheduleViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func button4Action(_ sender: UIButton) {
        
        if currentUserLogin == 4  {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            controller.studentData =  DataManager.sharedInstance.currentUser() as! [String : AnyObject]
            self.navigationController?.pushViewController(controller, animated: true)
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "LessonsViewController") as! LessonsViewController
            controller.fromDashBoard = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func button5Action(_ sender: UIButton) {
        
        if currentUserLogin == 4  {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MyGamesViewController") as! MyGamesViewController
            controller.studentData =  DataManager.sharedInstance.currentUser() as! [String : AnyObject]
            self.navigationController?.pushViewController(controller, animated: true)
            
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MediaViewController") as! MediaViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    @IBAction func button6Action(_ sender: UIButton) {
     
        if currentUserLogin == 4  {
            let storyboard = UIStoryboard(name: "Messages", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MessagesCustomeTabViewController") as! MessagesCustomeTabViewController
            self.navigationController?.pushViewController(controller, animated: true)
            
        }else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "AllBookingViewController") as! AllBookingViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    
    @IBAction func button7Action(_ sender: UIButton) {
       
        if currentUserLogin == 4 {
            var url = storeLink
            if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
                url = url + "\(id)"
            }
            UIApplication.shared.openURL(NSURL(string:url)! as URL)
            dismiss(animated: true, completion: nil)
            
        }else{
        showVideoGallery()
        }
    }
    
    @IBAction func button8Action(_ sender: UIButton) {
       
        if currentUserLogin == 4  {
        }else{
            recordVideo()
        }
    }
    
    @IBAction func button9Action(_ sender: UIButton) {
      
        let storyboard = UIStoryboard(name: "Messages", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "MessagesCustomeTabViewController") as! MessagesCustomeTabViewController
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func button10Action(_ sender: UIButton) {
        
        if currentUserLogin == 4  {
        }else{
            var url = storeLink
            if let id =  DataManager.sharedInstance.currentAcademy()!["AcademyID"] as? Int{
                url = url + "\(id)"
            }
            UIApplication.shared.openURL(NSURL(string:url)! as URL)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func rightMenuBtnAction(_ sender: UIBarButtonItem) {
         present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    func settingBGImageForCoach()  {
        
        containerHeightConstraint.constant = 830
        bgImageHeigthConstraint.constant = 375
        if currentTarget == "Jay Kelly Golf" || currentTarget == "Chris Ryan Golf" || currentTarget == "Playgolf" || currentTarget == "RLIgolf" {
            if currentTarget == "Chris Ryan Golf"{
                bgImage.image = UIImage(named: "chris_ryan_CoachDB")
                bgImageHeigthConstraint.constant = 270
                
            }else if currentTarget == "Playgolf"{
                bgImage.image = UIImage(named: "coaching_PG")
                bgImageHeigthConstraint.constant = 250

            }else if currentTarget == "RLIgolf"{
                var height = 375
                if Utility.isiPhonX(){
                    height = height + 60
                }

                bgImage.image = UIImage(named: "coachBGRli")
                bgImageHeigthConstraint.constant = CGFloat(height)

            }else{
                bgImage.image = UIImage(named: "jay_coach")
                bgImageHeigthConstraint.constant = 250
            }
            bgImage.contentMode = .scaleAspectFill
        }else if currentTarget == "3Hammers golf"{
            bgImage.image = UIImage(named: "coachingBg3Hammers")
            
            if Utility.isiPhonX(){
                bgImageHeigthConstraint.constant = 375
            }else{
                bgImageHeigthConstraint.constant = 310

            }
            bgImage.contentMode = .scaleAspectFill

        } else if currentTarget == "TPC Group"{
            bgImage.image = UIImage(named: "coachBgTPC")
            
            if Utility.isiPhonX(){
                bgImageHeigthConstraint.constant = 320
            }else{
                bgImageHeigthConstraint.constant = 310
                
            }
            bgImage.contentMode = .scaleAspectFill
    
        }else if currentTarget == "Suzy Whaley Golf"{
            bgImage.image = UIImage(named: "suziCoachDB")
            
            if Utility.isiPhonX(){
                bgImageHeigthConstraint.constant = 500
            }else{
                bgImageHeigthConstraint.constant = 375
                
            }
            bgImage.contentMode = .scaleAspectFill
        } else if currentTarget == "Thorpe Wood Golf"{
            //Thorpewood golf
            bgImage.image = UIImage(named: "coachBgThorpwood")
            
            if Utility.isiPhonX(){
                bgImageHeigthConstraint.constant = 250
            }else{
                bgImageHeigthConstraint.constant = 230
                
            }
            bgImage.contentMode = .scaleAspectFill
            
        }else if currentTarget == "Belfry Golf"{
            //Thorpewood golf
            bgImage.image = UIImage(named: "belfryCoachBG")
            
            if Utility.isiPhonX(){
                bgImageHeigthConstraint.constant = 320
            }else{
                bgImageHeigthConstraint.constant = 310
                
            }
            bgImage.contentMode = .scaleAspectFill
            
        }else{
                bgImage.image = UIImage(named: "result_model")
        }
        containerBtnHeightConstraint.constant = 600
    }
    
    func settingBGImageForPlayer() {
        
        containerHeightConstraint.constant = 590 + 120
        bgImageHeigthConstraint.constant = 568
        if currentTarget == "Jay Kelly Golf" || currentTarget == "Chris Ryan Golf" || currentTarget == "Playgolf" {
            if currentTarget == "Chris Ryan Golf"{
                bgImage.image = UIImage(named: "chris_ryan_StdentDB")
                bgImageHeigthConstraint.constant = 300
                
            }else if currentTarget == "Playgolf"{
                bgImage.image = UIImage(named: "student_PG")
                bgImageHeigthConstraint.constant = 250
            }else {
                //jay_Player
                bgImage.image = UIImage(named: "jay_coach")
                bgImageHeigthConstraint.constant = 270
            }
            bgImage.contentMode = .scaleAspectFill
        }else if currentTarget == "RLIgolf"{
            
            bgImage.image = UIImage(named: "studentBgRli")
            bgImageHeigthConstraint.constant = 230
            if Utility.isiPhonX(){
                bgImageHeigthConstraint.constant = 270
            }
            bgImage.contentMode = .scaleAspectFill
        }
        else if currentTarget == "3Hammers golf"{
            bgImage.image = UIImage(named: "studentBG3Hammers")
            bgImageHeigthConstraint.constant = 375
            bgImage.contentMode = .scaleAspectFill
            
        }else if currentTarget == "TPC Group"{
            bgImage.image = UIImage(named: "studentBGTPC")
            bgImageHeigthConstraint.constant = 375
            if Utility.isiPhonX(){
                bgImageHeigthConstraint.constant = 375
            }
            bgImage.contentMode = .scaleAspectFill

        }else if currentTarget == "Suzy Whaley Golf"{
            bgImage.image = UIImage(named: "suziStudentDB")
            bgImageHeigthConstraint.constant = 375
            if Utility.isiPhonX(){
                bgImageHeigthConstraint.constant = 450
            }
            bgImage.contentMode = .scaleAspectFill
            
        }else if currentTarget == "Thorpe Wood Golf"{

            bgImage.contentMode = .scaleAspectFill
            bgImage.image = UIImage(named: "playerBgThorpwood")

            
        }else if currentTarget == "Belfry Golf"{
            bgImage.image = UIImage(named: "belfryStudentBG")
            bgImageHeigthConstraint.constant = 130
//            if Utility.isiPhonX(){
//                bgImageHeigthConstraint.constant = 130
//            }
            bgImage.contentMode = .scaleAspectFill
            
        }else{
            bgImage.image = UIImage(named: "student_locker")
        }
        containerBtnHeightConstraint.constant = 360 + 120
    }

    func getBookedCoachSchedule()  {
        
        var coachId = ""
        if let id = DataManager.sharedInstance.currentUser()!["coachId"] as? Int{
            coachId = "\(id)"
        }
        
        NetworkManager.performRequest(type:.get, method: "Academy/GetCoachBookedSchedules/\(coachId)", parameter:nil, view: (UIApplication.getTopestViewController()!.view!), onSuccess: { (object) in
            print(object)
            switch object {
            case _ as NSNull:
                print("figured")
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            // do one thing
            case _ as [[String:AnyObject]]:
            self.eventsArray = object as! [[String:AnyObject]]
            DataManager.sharedInstance.synchScheduleInCalendar(eventsArray:self.eventsArray,view:(UIApplication.getTopestViewController()!.view!))
            default:
                break
            }
            
        }) { (error) in
            print(error!)
            self.showInternetError(error: error!)
        }
        
    }
    
    // MARK: - Private Methods
    private func captureVideo(source: Int) {
        
        let options: [String: Any] = ["shouldSaveVideoToGallery": (true),
                                      "videoSource": (source)]
        let callBackID = Utility.createCallBackID()
        let orbisCameraVC = OrbisCameraViewController.getCamera()
        orbisCameraVC?.delegate = self
        orbisCameraVC?.captureVideo(withCallbackID: callBackID, withOptions: options)
    }
    
    private func showGallery(with type: GalleryType) {
        
        let galleryViewController = GalleryViewController.init(nibName: "GalleryViewController", bundle: Bundle.main)
        galleryViewController.galleryType = type
        galleryViewController.delegate = self
        let navigationController = UINavigationController.init(rootViewController: galleryViewController)
//        self.present(navigationController, animated: true, completion: nil)
        
       UIApplication.getTopestViewController()?.present(navigationController, animated: true, completion: nil)
    }
    
    // MARK: - Wrapper Methods
    
    @objc func recordVideo() {
        captureVideo(source: 1)
    }
    
    @objc func importFromDevice() {
        captureVideo(source: 0)
    }
    
    @objc func showVideoGallery() {
        showGallery(with: GalleryTypeVideo)
    }
    
    @objc func showImageGallery() {
        
        showGallery(with: GalleryTypePhoto)
    }
    
    // MARK: - OrbisCameraViewController Delegate
    
    func orbisCameraVC(_ controller: OrbisCameraViewController!, didFinishSavingVideo info: [AnyHashable : Any]!, withCallBackID callBackID: String!) {
        
            Utility.showShapeViewController(with: info, presenter: self, animated: true, delegate: self)
            
        
    }
    
    // MARK: - GalleryViewController Delegate
    
    func galleryViewControllerDidSelectedAnalyse(_ videoInfo: [AnyHashable : Any]!) {
   
        Utility.showShapeViewController(with: videoInfo, presenter: self, animated: false, delegate: nil)
    }
    
    func galleryViewControllerDidSelectedTrim(_ videoInfo: [AnyHashable : Any]!) {
        
        Utility.showVideoPlayerController(with: videoInfo, presenter: self, animated: false, isOnline: false)
    }
    
    //holy // below methods are added after When using Record Video - once taken can we choose to save to a students Locker directly //can removed
    // MARK: - shapeView Controller Delegate

    func shapeViewController(_ controller: ShapeViewController!, didSelectSavetoLesson videoInfo: [AnyHashable : Any]!) {
        
        showUploadVC(info: videoInfo as! [String : Any], isFromSelectMedia: false)
    }
    
    private func showUploadVC(info: [String: Any], isFromSelectMedia: Bool) {
        
        let uploadVC = self.storyboard?.instantiateViewController(withIdentifier: "UploadMediaViewController") as! UploadMediaViewController
        uploadVC.mediaInfo = info
        uploadVC.mediaType = OrbisMediaType(rawValue: 1)
        if (isFromSelectMedia == true) {
            uploadVC.showSelectCategory = false
            uploadVC.showTitle = false
        }
        uploadVC.forStudentLocker = true
        self.navigationController?.pushViewController(uploadVC, animated: true)
    }
    
    
}


extension UIViewController{
    func isModal() -> Bool {
        
        if let navigationController = self.navigationController{
            if navigationController.viewControllers.first != self{
                return false
            }
        }
        
        if self.presentingViewController != nil {
            return true
        }
        
        if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        }
        
        if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        
        return false
    }
}
