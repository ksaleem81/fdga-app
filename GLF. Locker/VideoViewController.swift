//
//  VideoViewController.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/20/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SDWebImage
import SideMenu

class VideoViewController: UIViewController {

    var dataDictionary = [String:AnyObject]()
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var discriptionLbl: UILabel!
    @IBOutlet weak var footerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.activityIndicator.startAnimating()
        if let url = dataDictionary["Description"] as? String {
            discriptionLbl.text = url
        }
        
        var fileUrl = ""
         //check for youtube video
        if let url = dataDictionary["IsEmbedded"] as? Bool  {
        if let url = dataDictionary["ThumbnilURL"] as? String {
                fileUrl = url
            }
            if url {
                
            }else{
              
                fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
                fileUrl = baseUrlForVideo + fileUrl
            }
        }
        
        
        self.bgImage.sd_setImage(with: NSURL(string: fileUrl) as URL!, placeholderImage: UIImage(named:"photoNot"), options: SDWebImageOptions.refreshCached, completed: { (image, error, cacheType, url) in
            
            DispatchQueue.main.async (execute: {
                self.activityIndicator.isHidden = true
               // self.playBtn.isHidden = false
                if let _ = image{
                    
                    self.bgImage.image = image;
                }
                else{
                  self.bgImage.image = UIImage(named:"photoNot")
                    
                }
            });
            
        })
        
        
        var fileUrl2 = ""
        if let url = dataDictionary["FileURL"] as? String {
            fileUrl2 = url
        }
        
        if let url = dataDictionary["IsEmbedded"] as? Bool  {
           
            if url {
                self.playBtn.isHidden = true
                let videoView = UIWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 330))
                videoView.allowsInlineMediaPlayback = false
                videoView.mediaPlaybackRequiresUserAction = false
                self.view.addSubview(videoView)
                self.view.bringSubview(toFront: self.footerView)
                var url2 = fileUrl2
                let videoToken = fileUrl.components(separatedBy: "=")
                    if videoToken.count > 1 {
                 url2 = "https://www.youtube.com/embed/\(videoToken[1])"
                    }else{
                     let videoToken2 = fileUrl2.components(separatedBy: "/")
                        if videoToken2.count > 1 {
                    url2 = "https://www.youtube.com/embed/\(videoToken2[videoToken2.count-1])"
                        }
                }
             
                videoView.loadHTMLString("<iframe width=\"\(self.view.frame.size.width)\" height=\"330\" src=\"\(url2)\" frameborder=\"0\" allowfullscreen></iframe>" , baseURL: nil)
                
            }else{
                
            
            }
        }

        
        //settingRighMenuBtn()
    }
    
    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(VideoViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func playVideoAction(_ sender: UIButton) {
        if let url = dataDictionary["IsEmbedded"] as? Bool  {
            if url {
            }else{
                playVideo()
            }
        }
    }

    func playVideo()  {
        
        var fileUrl = ""
        if let url = dataDictionary["FileURL"] as? String {
            fileUrl = url
        }
        
               fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
            fileUrl = baseUrlForVideo + fileUrl
        
        
        
        let destination = AVPlayerViewController()
     

        let url = NSURL(string: fileUrl)!
        destination.player = AVPlayer(url: url as URL)
        self.present(destination, animated: true) {
             destination.player?.play()
        }
       
        NotificationCenter.default.addObserver(self, selector:#selector(VideoViewController.playerDidFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: destination.player?.currentItem)
        
    }
    
    //playVideo
    
    func playerItemDidReachEnd(notification: NSNotification) {
        let p: AVPlayerItem = notification.object as! AVPlayerItem
        p.seek(to: kCMTimeZero)
    }

    

 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    if segue.identifier == "playVideo" {
        
            let destination = segue.destination as! AVPlayerViewController
        
        var fileUrl = ""
        if let url = dataDictionary["FileURL"] as? String {
            fileUrl = url
        }
        //check for youtube video
        if let url = dataDictionary["IsEmbedded"] as? Bool  {
            if url {
                
            }else{
                
                fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
                fileUrl = baseUrlForVideo + fileUrl
            }
        }
        
            let url = NSURL(string: fileUrl)!
            destination.player = AVPlayer(url: url as URL)
            destination.player?.play()
        NotificationCenter.default.addObserver(self, selector:#selector(VideoViewController.playerDidFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: destination.player?.currentItem)
        
    }
 }
 
    @objc func playerDidFinishPlaying(note: NSNotification) {
        print("Video Finished")
        self.dismiss(animated: true, completion:nil)
    }
 
}

/*
 func playVideo(rate1:Double)  {
 
 var fileUrl = ""
 if let url = dataDictionary["FileURL"] as? String {
 fileUrl = url
 }
 
 fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
 fileUrl = baseUrlForVideo + fileUrl
 
 
 let url = NSURL(string: fileUrl)!
 let rate = rate1;
 let asset = AVAsset.init(url: url as URL)
 
 let composition = AVMutableComposition()
 
 try?composition.insertTimeRange(CMTimeRangeMake(kCMTimeZero, asset.duration), of: asset, at:kCMTimeZero)
 composition.scaleTimeRange(CMTimeRangeMake(kCMTimeZero, asset.duration), toDuration: CMTimeMultiplyByFloat64(asset.duration, 1.0 / rate))
 
 
 let playerItem = AVPlayerItem.init(asset: composition)
 destination.player = AVPlayer.init(playerItem: playerItem)//AVPlayer(url: url as URL
 let viewOF = UIButton.init(frame: CGRect(x: 0, y: (self.appDelegate.window?.frame.size.height)!-50, width:50, height: 50))
 viewOF.addTarget(self, action: #selector(changeRateBtn(button:)), for: .touchUpInside)
 viewOF.backgroundColor = UIColor.gray
 viewOF.setTitle("Speed", for: .normal)
 
 destination.view.addSubview(viewOF)
 
 self.present(destination, animated: true) {
 self.destination.player?.play()
 }
 
 NotificationCenter.default.addObserver(self, selector:#selector(VideoViewController.playerDidFinishPlaying),
 name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: destination.player?.currentItem)
 
 
 }
 
 
 func changeRateBtn(button:UIButton)  {
 // self.dismiss(animated: true, completion:nil)
 self.destination.player?.pause()
 let actionSheetController: UIAlertController = UIAlertController(title:"Change Video Speed", message:nil, preferredStyle: .actionSheet)
 let logOutAction: UIAlertAction = UIAlertAction(title: "CLOSE", style: .destructive) { action -> Void in
 self.destination.player?.play()
 //Do some stuff
 }
 let photoAction: UIAlertAction = UIAlertAction(title: "1/1", style: .default) { action -> Void in
 self.changeVideoSpeed(rate1: 1.0)
 
 }
 let videoAction: UIAlertAction = UIAlertAction(title: "1/2", style: .default) { action -> Void in
 self.changeVideoSpeed(rate1: 0.50)
 
 
 }
 
 let drilAction: UIAlertAction = UIAlertAction(title: "1/4", style: .default) { action -> Void in
 self.changeVideoSpeed(rate1: 0.25)
 
 }
 
 actionSheetController.addAction(photoAction)
 actionSheetController.addAction(videoAction)
 actionSheetController.addAction(drilAction)
 actionSheetController.addAction(logOutAction)
 if let popoverPresentationController = actionSheetController.popoverPresentationController {
 popoverPresentationController.sourceView = self.view
 popoverPresentationController.sourceRect = CGRect(x:self.view.bounds.size.width, y: 0,width:1.0, height:1.0)
 }
 
 actionSheetController.modalPresentationStyle = .popover
 destination.present(actionSheetController, animated: true, completion: nil)
 
 }
 func changeVideoSpeed(rate1:Double) {
 var fileUrl = ""
 if let url = self.dataDictionary["FileURL"] as? String {
 fileUrl = url
 }
 
 fileUrl = fileUrl.replacingOccurrences(of: "~", with: "", options: .regularExpression)
 fileUrl = baseUrlForVideo + fileUrl
 
 
 let url = NSURL(string: fileUrl)!
 let rate = rate1
 let asset = AVAsset.init(url: url as URL)
 
 let composition = AVMutableComposition()
 
 try?composition.insertTimeRange(CMTimeRangeMake(kCMTimeZero, asset.duration), of: asset, at:kCMTimeZero)
 composition.scaleTimeRange(CMTimeRangeMake(kCMTimeZero, asset.duration), toDuration: CMTimeMultiplyByFloat64(asset.duration, 1.0 / rate))
 
 
 let playerItem = AVPlayerItem.init(asset: composition)
 self.destination.player = AVPlayer.init(playerItem: playerItem)
 self.destination.player?.play()
 }
 
*/



