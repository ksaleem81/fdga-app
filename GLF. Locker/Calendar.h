#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <EventKitUI/EventKitUI.h>
#import <EventKit/EventKit.h>

@interface Calendar : CDVPlugin<UIAlertViewDelegate>
{
    BOOL _isModifying;
}

@property (nonatomic, strong) EKEventStore* eventStore;
@property (nonatomic, strong) NSArray *fetchedEvents;


- (void)initEventStoreWithCalendarCapabilities;

-(NSArray*)findEKEventsWithTitle: (NSString *)title
                        location: (NSString *)location
                           notes: (NSString *)notes
                       startDate: (NSDate *)startDate
                         endDate: (NSDate *)endDate
                        calendar: (EKCalendar *) calendar;

- (void)createCalendar:(CDVInvokedUrlCommand*)command;
- (void)deleteCalendar:(CDVInvokedUrlCommand*)command;

- (void)createEventWithOptions:(CDVInvokedUrlCommand*)command;
- (void)createEventInteractively:(CDVInvokedUrlCommand*)command;
- (void)createEventInNamedCalendar:(CDVInvokedUrlCommand*)command;
- (void)createEventsInNamedCalendar:(CDVInvokedUrlCommand*)command;

- (void)modifyEvent:(CDVInvokedUrlCommand*)command;
- (void)modifyEventInNamedCalendar:(CDVInvokedUrlCommand*)command;

- (void)findEvent:(CDVInvokedUrlCommand*)command;
- (void)findAllEventsInNamedCalendar:(CDVInvokedUrlCommand*)command;

- (void)listCalendars:(CDVInvokedUrlCommand*)command;

- (void)deleteEvent:(CDVInvokedUrlCommand*)command;
- (void)deleteEventFromNamedCalendar:(CDVInvokedUrlCommand*)command;

- (void)startingEventsModification:(CDVInvokedUrlCommand*)command;
- (void)eventsModificationEnded:(CDVInvokedUrlCommand*)command;




- (void) deleteEventFromCalendar:(NSString*)calendarName withTitle:(NSString*)title;
- (void) modifyEventWithCalendar:(NSString*)calendarName title:(NSString*)title newTitle:(NSString*)nTitle newLocation:(NSString*)nlocation newNotes:(NSString*) nnotes newStartDate:(NSDate*)nStartDate newEndDate:(NSDate*)nEndDate;



@end
