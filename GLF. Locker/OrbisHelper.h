//
//  OrbisHelper.h
//  GLF. Locker
//
//  Created by Jawad Ahmed on 08/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrbisHelper : NSObject

+ (void)getThumbnailOfVideo:(NSString *)videoPath completion:(void(^)(NSError *error, NSDictionary *info))completion;

@end
