//
//  RefreshingViewController.swift
//  GLF. Locker
//
//  Created by Kashif Jawad on 22/01/2018.
//  Copyright © 2018 Nasir Mehmood. All rights reserved.
//

import UIKit


class RefreshingViewController: UIViewController {

    typealias FailureBlock = (NSError?) -> Void
    typealias TSuccessBlock = (AnyObject?) -> Void

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

     func refreshData(acedmyId:String, onSuccess: @escaping TSuccessBlock , onFailure: @escaping FailureBlock )  {
        
        if acedmyId == "" {
            return
        }
        
        NetworkManager.performRequest(type:.get, method: "academy/SearchAcademyByID_native/\(acedmyId)", parameter: nil, view: ((UIApplication.getTopestViewController()!.view!)), onSuccess: { (object) in
            print(object!)
            switch object {
            case   _ as NSNull :
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:self)
                return
            case _ as [String:AnyObject]:
                DataManager.sharedInstance.printAlertMessage(message:"\(object!)", view:UIApplication.getTopestViewController()!)
                return
            default: break
            }
           let data = object as! [[String:AnyObject]]
            onSuccess(data as AnyObject)
            
        }) { (error) in
            self.showInternetError(error: error!)
        }
    }

}
