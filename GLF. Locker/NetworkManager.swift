//
//  NetworkManager.swift
//  MyAlist
//
//  Created by Apple PC on 08/09/2016.
//  Copyright © 2016 ArhamSoft. All rights reserved.
//

import UIKit
import Alamofire


class NetworkManager: NSObject {

    var error : String = ""
    var responseObject : AnyObject
    typealias FailureBlock = (NSError?) -> Void
    typealias TSuccessBlock = (AnyObject?) -> Void
    
    override var description : String    {
        return "\nResponse { response = \(responseObject), error = \(error)}"
    }
   
    required convenience init(responseObject:AnyObject? , error:String? ) {
        self.init()
        self.error = error!
        self.responseObject = responseObject!
    }
    
    override init()   {
        error = ""
        responseObject = "" as AnyObject
    }
    
    class func getJSONFromServer(_ url: URL,completion:@escaping (_ dic:NSDictionary)->Void ) {
       
        let urlDomain = "http://clients.arhamsoft.com/shareads/dev/api/register"
        // build parameters
        let parameters  = ["first_name":"kashif222","last_name": "Jawad222", "email": "kashif@gmail.com", "account_type": "1"]
        
        Alamofire.request(urlDomain, method: .post, parameters: parameters, encoding: JSONEncoding.default)
                        .validate { request, response, data in
             
                            print(response)
                            print(request!)
                            print(data!)
                return .success
            }
            .responseJSON { response in
                debugPrint(response)
        }
        
    }
    
    class func performRequest(type: HTTPMethod , method:String, parameter: [String: AnyObject]?, view:UIView!, onSuccess: @escaping TSuccessBlock , onFailure: @escaping FailureBlock ){
      
        
        let connected: Bool = Reachability.forInternetConnection().isReachable()
        
        if (connected == true) {
            if view != nil {
                MBProgressHUD.showAdded(to:view, animated: true)
            }
        }
        if parameter != nil {
//             print(parameter)
        }
       
        let urlDomain = baseUrl + method
        print(urlDomain)

        var headers   = HTTPHeaders()
        
        headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        //after token basedathentication
        
        headers = Utility.getAthenticatedHeader(method: method)
        //till here
        
        Alamofire.request(urlDomain, method:type, parameters: parameter, headers: headers)
            .validate { request, response, data in
                
                print(response)
                print(request!)
                print(data!)
                return .success
            }.responseJSON { response in
                
                  
                    if response.result.error == nil {
                              if view != nil {
                                MBProgressHUD.hideAllHUDs(for:view, animated: true )
                        }
                     
                        if  response.result.isSuccess  {
                            
                            if let data =  response.result.value as? [String:AnyObject],data["Message"] as? String  == "Authorization has been denied for this request."{
                                Utility.authorizeAlert()
                            }else{
                                
                                onSuccess(response.result.value as AnyObject)
                                
                            }

                        }else{
                            onFailure(response.result.error as NSError?)
                        }
        
                    }else{
                        debugPrint(response.result.error!)
                        onFailure(response.result.error as NSError?)
                        if view != nil {
                            MBProgressHUD.hideAllHUDs(for: view, animated: true )
                        }
                    }
            }
        }
    
    func isAppAlreadyLaunchedOnce()->Bool{
        let defaults = UserDefaults.standard
        
        if let isAppAlreadyLaunchedOnce = defaults.string(forKey: "isAppAlreadyLaunchedOnce"){
            print("App already launched : \(isAppAlreadyLaunchedOnce)")
            return true
        }else{
            defaults.set(true, forKey: "isAppAlreadyLaunchedOnce")
            print("App launched first time")
            return false
        }
    }
    
    class func performRequestForYouTubeUpload(type: HTTPMethod , method:String, parameter: [String: AnyObject]?, view:UIView!, onSuccess: @escaping TSuccessBlock , onFailure: @escaping FailureBlock ){
        
        let connected: Bool = Reachability.forInternetConnection().isReachable()
        
        if (connected == true) {
            if view != nil {
                MBProgressHUD.showAdded(to:view, animated: true)
            }
        }
    let urlDomain = baseUrl + method
        print(urlDomain)
    let url = NSURL(string: urlDomain) //Remember to put ATS exception if the URL is not https
    let request = NSMutableURLRequest(url: url! as URL)
    request.addValue("application/json", forHTTPHeaderField: "Content-Type") //Optional
    request.httpMethod = "PUT"
    let session = URLSession(configuration:URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
        
        if let academyID = parameter?["IsEmbedded"] as? String{
            request.setValue(academyID, forHTTPHeaderField: "IsEmbedded")
        }
        if let academyID = parameter?["AcademyID"] as? String{
            request.setValue(academyID, forHTTPHeaderField: "AcademyID")
        }
        if let academyID = parameter?["MediaTypeID"] as? String{
            request.setValue(academyID, forHTTPHeaderField: "MediaTypeID")
        }

        if let academyID = parameter?["FileURL"] as? String{
            request.setValue(academyID, forHTTPHeaderField: "FileURL")
        }

        if let academyID = parameter?["Description"] as? String{
            request.setValue(academyID, forHTTPHeaderField: "Description")
        }

        if let academyID = parameter?["CreatedBy"] as? String{
            request.setValue(academyID, forHTTPHeaderField: "CreatedBy")
        }
        
        if let academyID = parameter?["CreationDate"] as? String{
            request.setValue(academyID, forHTTPHeaderField: "CreationDate")
        }

        if let academyID = parameter?["skillCategory"] as? String{
            request.setValue(academyID, forHTTPHeaderField: "skillCategory")
        }

        if let academyID = parameter?["skillTypes"] as? String{
            request.setValue(academyID, forHTTPHeaderField: "skillTypes")
        }
        
    
    let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
        
        if error != nil {
            
            //handle error
            debugPrint(error!)
            onFailure(error as NSError?)
            if view != nil {
                MBProgressHUD.hideAllHUDs(for: view, animated: true )
            }
        }
        else {
            
            let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("Parsed JSON: '\(String(describing: jsonStr))'")
            if view != nil {
                MBProgressHUD.hideAllHUDs(for:view, animated: true )
            }
            onSuccess(jsonStr! as AnyObject)
        }
    }
    dataTask.resume()

    }
    
    class func sendJsonToServer(_ url: String, image : UIImage?, fileNameStr : String,json:NSDictionary,completion:@escaping (_ dic:NSDictionary)->Void,failure:@escaping (_ error:NSError)->Void) {
        /*
        if(!NetworkManager.isconnectedToNetwork()){
         //   UIAlertView.showNetworkErrorMessage()
            SVProgressHUD.dismiss()
            return
        }
 */
       let urlDomain = URL(string:"http://clients.arhamsoft.com/shareads/dev/api/" + url )
     
        do {
            
            Alamofire.upload(multipartFormData: {  multipartFormData in
                if let _image = image {
                    if let imageData = UIImageJPEGRepresentation(_image, 0.5) {
                       
                        multipartFormData.append(imageData, withName: "\(fileNameStr)", fileName: "\(fileNameStr).png", mimeType: "image/png")
                    }
                }
                for (key, value) in json {
                 
//                   text.data(using: String.Encoding.utf8)
                    
                    multipartFormData.append((value as! String).data(using:String.Encoding.utf8)!, withName: key as! String)
                 // multipartFormData.append(value.dataUsingEncoding(String.Encoding.utf8)!, withName:  key as! String)
                    
                }
                }, to: urlDomain!, encodingCompletion: { encodingResult in
                    //print(encodingResult)
                    switch encodingResult {
                    
                    case .success(let upload, let streamingFromDisk , let streamingFileUrl):
                        debugPrint(streamingFromDisk)
                        debugPrint(streamingFileUrl!)
                        upload.responseJSON { responce  in
                            
                        print(responce.result.value!)
                            if let responceDic = responce.result.value as? [String: AnyObject] {
                                let dictionary = responceDic as NSDictionary
                                completion(dictionary)
                            }else {
                                
                                let errorDictionary : [String : String] = [NSLocalizedDescriptionKey : "Some thing goes wrong"]
                                let error1 : NSError? = NSError(domain: "Error", code: 404, userInfo: errorDictionary)
                                failure(error1!)
                                
                            }
                            
                        }
                    case .failure(let encodingError):
                        debugPrint(encodingError)
                        break
                    }
            })
        }
 }
    
    /*class func isconnectedToNetwork()->Bool{
        let reachability = Reachability.reachabilityForInternetConnection()
        
        if(reachability!.isReachable()){
            
            return true
        }
        return false
    }*/

    class func downloadFile(url:String){
        
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        Alamofire.download(
            url,
            method: .get,
            parameters: nil,
            encoding: JSONEncoding.default,
            headers: nil,
            to: destination).downloadProgress(closure: { (progress) in
                print(progress)
            }).response(completionHandler: { (DefaultDownloadResponse) in
                //here you able to access the DefaultDownloadResponse
                //result closure
                print(DefaultDownloadResponse)
            })

    }
    
}
