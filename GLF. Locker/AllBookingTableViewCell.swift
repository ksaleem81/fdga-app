//
//  AllBookingTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 2/6/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit

class AllBookingTableViewCell: UITableViewCell {

    @IBOutlet weak var programImage: UIImageView!
    @IBOutlet weak var programTitleLbl: UILabel!
      @IBOutlet weak var programSubTitleLbl: UILabel!

    @IBOutlet weak var durationLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var startTimeLbl: UILabel!
    @IBOutlet weak var lessonDateLbl: UILabel!
    @IBOutlet weak var lessonDateValueLbl: UILabel!
    @IBOutlet weak var bookingLbl: UILabel!
    @IBOutlet weak var bookingValueLbl: UILabel!
    @IBOutlet weak var paymentTypLbl: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
  

}
