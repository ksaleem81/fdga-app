//
//  ScheduleFooterTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 1/25/17.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit

class ScheduleFooterTableViewCell: UITableViewCell {

    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
