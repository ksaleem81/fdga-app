//
//  MapTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit

protocol textFieldDelegate {
    func textFieldTextReturn(text:String,tag:Int)
 
}

class GameScoreSubTableViewCell: UITableViewCell,UITextFieldDelegate {

    var delegate:textFieldDelegate?
    @IBOutlet weak var companyLbl: UILabel!
    @IBOutlet weak var radioBtn: UIButton!
   
    @IBOutlet weak var scoreLbl: UILabel!
    @IBOutlet weak var field: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.field.delegate = self
        // Initialization code
     
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
   
    func textFieldDidEndEditing(_ textField: UITextField) {
        print(textField.text!)
        if (textField.text ?? "").isEmpty {
            return
        }
        if Int(textField.text!)! > 10 {
            DataManager.sharedInstance.printAlertMessage(message:"Ranking value should be b/w 0 to 10", view:UIApplication.getTopestViewController()!)
        textField.text = "0"
        return
        }

     self.delegate?.textFieldTextReturn(text: textField.text!,tag: textField.tag)
    }
  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
