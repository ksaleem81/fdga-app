    //
//  BLCameraController.m
//  AVCam
//
//  Created by Nasir Mehmood on 7/27/14.
//  Copyright (c) 2014 Nasir Mehmood. All rights reserved.
//

#import "BLCameraController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "OrbisCameraViewController.h"
//#import "AppDelegate.h"
#import "GlobalClass.h"

#define kBitsPerMbits 1048576


@interface BLCameraController ()
{
    UIView *_overlayView;
    
    AVCaptureSession *_captureSession;
    NSString *_captureSessionPreset;
    NSString *_sessionPreset;
    AVCaptureDevice *_captureDevice;
    AVCaptureDeviceInput *_captureVideoInput;
    AVCaptureDeviceInput *_captureAudioInput;
    AVCaptureDevicePosition _captureDevicePosition;
    NSString *_capturedFilePath;
    NSURL *_capturedFileURL;
    int _captureDuation;
    
    AVCaptureVideoDataOutput *_captureVideoOutput;
    AVCaptureVideoPreviewLayer *_previewLayer;

    AVCaptureConnection *_audioConnection;
    AVCaptureConnection *_videoConnection;
    
    AVAssetWriter *_assetWriter;
    AVAssetWriterInput *_assetWriterAudioIn;
    AVAssetWriterInput *_assetWriterVideoIn;
    dispatch_queue_t _movieWritingQueue;
    
    AVCaptureVideoOrientation _videoOrientation;
    AVCaptureVideoOrientation _referenceOrientation;

    CMVideoCodecType _videoType;

    BOOL _readyToRecordAudio;
    BOOL _readyToRecordVideo;
    BOOL _recordingWillBeStarted;
    BOOL _recordingWillBeStopped;
    
    BOOL _recording;
    
    
    int _capturingBitRate;
    UIButton *_recordButton;
}

@end

@implementation BLCameraController

@synthesize delegate;

- (id) init
{
    self=[super init];
    if(self)
    {
//        _capturedFilePath=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"capture%d.mov", (int)[NSDate timeIntervalSinceReferenceDate]]];
        _capturedFilePath=[[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:[NSString stringWithFormat:@"capture%d.mp4", (int)[NSDate timeIntervalSinceReferenceDate]]];
        _capturedFileURL=[NSURL fileURLWithPath:_capturedFilePath];
        
        [self setSessionPreset:AVCaptureSessionPreset640x480];
        [self setCaptureDevicePosition:AVCaptureDevicePositionBack];
        
        _captureDuation=10*60;   // 10 min
        _captureDevice=[self getVideoCaptureDeviceWithPosition:_captureDevicePosition];
        
        _recording=NO;
        _readyToRecordAudio=NO;
        _readyToRecordVideo=NO;
        _recordingWillBeStopped=NO;
        _recordingWillBeStarted=NO;
        
//        _referenceOrientation = AVCaptureVideoOrientationPortrait;
        [self changeVideoConnectionOrientation];
    }
    
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView
{
    [super loadView];
    self.view=[[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL) shouldAutorotate
{
    if ( _rotate)
    {
          return YES;
    }
    
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (AVCaptureVideoOrientation) getCurrentOrientation
{
    UIDeviceOrientation currentDeviceOrientation = [[UIDevice currentDevice] orientation];
    
    if (currentDeviceOrientation==UIDeviceOrientationUnknown) {
        GlobalClass *appDelegate = [GlobalClass sharedInstance];
        currentDeviceOrientation = [UIDevice currentDevice].orientation; //appDelegate._currentDeviceOrientation;
    }
    
//    CGRect layerRect = [[[self view] layer] bounds];
//    [_previewLayer setFrame:layerRect];
//    _previewLayer.videoGravity=AVLayerVideoGravityResizeAspectFill;
    
    NSLog(@"currentDeviceOrientation: %d", currentDeviceOrientation);
    switch (currentDeviceOrientation) {
        case UIInterfaceOrientationLandscapeLeft:
        {
            return AVCaptureVideoOrientationLandscapeLeft;
        }
        break;
        case UIInterfaceOrientationLandscapeRight:
        {
            return AVCaptureVideoOrientationLandscapeRight;
        }
        break;
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            return AVCaptureVideoOrientationPortraitUpsideDown;
        }
        break;
        
        default:
        {
            NSLog(@"self.interfaceOrientation: %d", self.interfaceOrientation);
            switch(self.interfaceOrientation)
            {
                case UIInterfaceOrientationUnknown:
                {
                    return AVCaptureVideoOrientationPortrait;
                }
                    break;
                case UIInterfaceOrientationPortrait:
                {
                    return AVCaptureVideoOrientationPortrait;
                }
                    break;
                case UIInterfaceOrientationLandscapeLeft:
                {
                    return AVCaptureVideoOrientationLandscapeLeft;
                }
                    break;
                case UIInterfaceOrientationLandscapeRight:
                {
                    return AVCaptureVideoOrientationLandscapeRight;
                }
                    break;
                default:
                {
                    return AVCaptureVideoOrientationPortrait;
                }
                    break;
            }
        }
        break;
    }
}


//- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
//    
//    [self changeOrientation:toInterfaceOrientation];
//}
//
//-(void)changeOrientation:(UIInterfaceOrientation)toInterfaceOrientation
//{
//    if (_previewLayer) {
//        if (toInterfaceOrientation==UIInterfaceOrientationPortrait) {
//            _previewLayer.transform = CATransform3DMakeRotation(0, 0.0, 0.0, 1.0);
//        } else if (toInterfaceOrientation==UIInterfaceOrientationLandscapeLeft) {
//            _previewLayer.transform =CATransform3DMakeRotation((M_PI/2), 0.0, 0.0, 1.0);
//        } else if (toInterfaceOrientation==UIInterfaceOrientationLandscapeRight) {
//            _previewLayer.transform = CATransform3DMakeRotation((-M_PI/2), 0.0, 0.0, 1.0);
//        }
//        _previewLayer.frame = self.view.bounds;
//    }
//}


#pragma mark - Custom Methods

- (void) setOverlayView:(UIView*)overlayView
{
    if(_overlayView)
    {
        [_overlayView removeFromSuperview];
    }

    
    _overlayView=overlayView;
//    [self.view addSubview:_overlayView];
//    if(_previewLayer)
//    {
//        [_overlayView.layer addSublayer:_previewLayer];
//    }
    
}

- (void) setSessionPreset:(NSString*)sessionPreset
{
    _sessionPreset=sessionPreset;
    [self setCaptureSessionPreset:sessionPreset];
}


- (void) setCaptureSessionPreset:(NSString*)captureSessionPreset
{
    _captureSessionPreset=captureSessionPreset;
    
//    AVVideoProfileLevelH264High41
    
//    _captureSessionPreset=AVCaptureSessionPreset640x480;
    
    _capturingBitRate=2*kBitsPerMbits;
    
    if([captureSessionPreset isEqualToString:AVCaptureSessionPreset352x288]) {
        _capturingBitRate=2*kBitsPerMbits;
    }
    else if([captureSessionPreset isEqualToString:AVCaptureSessionPreset640x480]) {
        _capturingBitRate=2.5*kBitsPerMbits;
//        _capturingBitRate=4*kBitsPerMbits;
    }
    else if([captureSessionPreset isEqualToString:AVCaptureSessionPreset1280x720]) {
        _capturingBitRate=7*kBitsPerMbits;
//        _capturingBitRate=10*kBitsPerMbits;
    }
    else if([captureSessionPreset isEqualToString:AVCaptureSessionPreset1920x1080]) {
        _capturingBitRate=17*kBitsPerMbits;
    }
    else if([captureSessionPreset isEqualToString:AVCaptureSessionPresetHigh]) {
        _capturingBitRate=8*kBitsPerMbits;
    }
    else if([captureSessionPreset isEqualToString:AVCaptureSessionPresetMedium]) {
        _capturingBitRate=8*kBitsPerMbits;
    }
    
}

- (void) setCaptureDevicePosition:(AVCaptureDevicePosition)captureDevicePosition
{
    _captureDevicePosition=captureDevicePosition;
    
    if(_captureSession)
    {
        [_captureSession beginConfiguration];
        [_captureSession removeInput:_captureVideoInput];
        _captureVideoInput=nil;
        
        _captureSession.sessionPreset = AVCaptureSessionPresetLow;
        
        _captureDevice=[self getVideoCaptureDeviceWithPosition:_captureDevicePosition];
        _captureVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:_captureDevice error:nil];
        if ([_captureSession canAddInput:_captureVideoInput])
            [_captureSession addInput:_captureVideoInput];
        
        
        if(_captureVideoOutput)
            [_captureSession removeOutput:_captureVideoOutput];
        _captureVideoOutput = [[AVCaptureVideoDataOutput alloc] init];
        [_captureVideoOutput setAlwaysDiscardsLateVideoFrames:YES];
        dispatch_queue_t videoCaptureQueue = dispatch_queue_create("Video Capture Queue", DISPATCH_QUEUE_SERIAL);
        [_captureVideoOutput setSampleBufferDelegate:self queue:videoCaptureQueue];
        
        if ([_captureSession canAddOutput:_captureVideoOutput])
            [_captureSession addOutput:_captureVideoOutput];
        
        AVCaptureVideoOrientation videoOrientation=[self getCurrentOrientation];
        _videoConnection = [_captureVideoOutput connectionWithMediaType:AVMediaTypeVideo];
        [_videoConnection setVideoOrientation:videoOrientation];
        _videoOrientation = [_videoConnection videoOrientation];
        
        _captureSessionPreset=_sessionPreset;
        if([_captureSession canSetSessionPreset:_captureSessionPreset])
            _captureSession.sessionPreset = _captureSessionPreset;
        else
        {
            if([_captureSessionPreset isEqualToString:AVCaptureSessionPreset1280x720]) {
                [self setCaptureSessionPreset:AVCaptureSessionPresetHigh];
            }
            else
            {
                [self setCaptureSessionPreset:AVCaptureSessionPresetMedium];
            }
            
            if([_captureSession canSetSessionPreset:_captureSessionPreset])
                _captureSession.sessionPreset = _captureSessionPreset;
        }
        
        [_captureSession commitConfiguration];
        
        [self configureCameraForHighestFrameRate:_captureDevice];
    }
}

- (void)configureCameraForHighestFrameRate:(AVCaptureDevice *)device
{
//    AVCaptureDeviceFormat *bestFormat = nil;
//    AVFrameRateRange *bestFrameRateRange = nil;
//    for ( AVCaptureDeviceFormat *format in [device formats] ) {
//        for ( AVFrameRateRange *range in format.videoSupportedFrameRateRanges ) {
//            if ( range.maxFrameRate > bestFrameRateRange.maxFrameRate ) {
//                bestFormat = format;
//                bestFrameRateRange = range;
//            }
//        }
//    }
    
    AVCaptureDeviceFormat *bestFormat = device.activeFormat;
    AVFrameRateRange *bestFrameRateRange = nil;
    for ( AVFrameRateRange *range in bestFormat.videoSupportedFrameRateRanges ) {
        if ( range.maxFrameRate > bestFrameRateRange.maxFrameRate ) {
            bestFrameRateRange = range;
        }
    }

    
    
    if ( bestFormat ) {
        if ( [device lockForConfiguration:NULL] == YES ) {
//            device.activeFormat = bestFormat;
            device.activeVideoMinFrameDuration = bestFrameRateRange.minFrameDuration;
            device.activeVideoMaxFrameDuration = bestFrameRateRange.minFrameDuration;
            [device unlockForConfiguration];
        }
    }
}


- (void) setCapturedFilePath:(NSString*)capturedFilePath
{
    _capturedFilePath=capturedFilePath;
    _capturedFileURL=[NSURL fileURLWithPath:_capturedFilePath];
}

- (void) setCaptureDuation:(int)captureDuation
{
    _captureDuation=captureDuation;
}

- (BOOL) isFlashAvailable
{
    if ([_captureDevice hasTorch] && [_captureDevice hasFlash]){
        return YES;
    }
    return NO;
}

- (void) turnFlash:(BOOL)on_off;
{
    if ([self isFlashAvailable]){
        
        [_captureDevice lockForConfiguration:nil];
        if (on_off) {
            [_captureDevice setTorchMode:AVCaptureTorchModeOn];
            [_captureDevice setFlashMode:AVCaptureFlashModeOn];
        } else {
            [_captureDevice setTorchMode:AVCaptureTorchModeOff];
            [_captureDevice setFlashMode:AVCaptureFlashModeOff];
        }
        [_captureDevice unlockForConfiguration];
    }
}


- (AVCaptureDevice *)getVideoCaptureDeviceWithPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
        if ([device position] == position)
            return device;
    
    return nil;
}

- (AVCaptureDevice *)getAudioCaptureDevice
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio];
    if ([devices count] > 0)
        return [devices objectAtIndex:0];
    
    return nil;
}


- (CGAffineTransform)transformFromCurrentVideoOrientationToOrientation:(AVCaptureVideoOrientation)orientation
{
	CGAffineTransform transform = CGAffineTransformIdentity;
    
//    _videoOrientation=[_videoConnection videoOrientation];
    _videoOrientation=_previewLayer.connection.videoOrientation;
    
	// Calculate offsets from an arbitrary reference orientation (portrait)
	CGFloat orientationAngleOffset = [self angleOffsetFromPortraitOrientationToOrientation:orientation];
	CGFloat videoOrientationAngleOffset = [self angleOffsetFromPortraitOrientationToOrientation:_videoOrientation];
	
	// Find the difference in angle between the passed in orientation and the current video orientation
	CGFloat angleOffset = orientationAngleOffset - videoOrientationAngleOffset;
	transform = CGAffineTransformMakeRotation(angleOffset);
	
    if(_captureDevicePosition==AVCaptureDevicePositionFront && orientation!=AVCaptureVideoOrientationLandscapeLeft && orientation!=AVCaptureVideoOrientationLandscapeRight)
    {
//        transform=CGAffineTransformMake( 1, 0, 0, 0, 1, 0 );
        transform=CGAffineTransformMake( 1, 0, 0, 1, 0, 0 );
        angleOffset=angleOffset*-1.0;
        transform = CGAffineTransformConcat(CGAffineTransformMakeRotation(angleOffset), transform);
//        transform = CGAffineTransformConcat(transform, CGAffineTransformMakeScale(1.0, -1.0));
//        transform = CGAffineTransformConcat(CGAffineTransformMakeRotation(angleOffset), CGAffineTransformMakeScale(1.0, -1.0));
//        transform = CGAffineTransformConcat(transform2, transform);
    }
    
	return transform;
}

- (CGFloat)angleOffsetFromPortraitOrientationToOrientation:(AVCaptureVideoOrientation)orientation
{
	CGFloat angle = 0.0;
	
	switch (orientation) {
		case AVCaptureVideoOrientationPortrait:
			angle = 0.0;
			break;
		case AVCaptureVideoOrientationPortraitUpsideDown:
			angle = M_PI;
			break;
		case AVCaptureVideoOrientationLandscapeRight:
			angle = -M_PI_2;
			break;
		case AVCaptureVideoOrientationLandscapeLeft:
			angle = M_PI_2;
			break;
		default:
			break;
	}
    
	return angle;
}

- (void)saveMovieToCameraRoll
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library writeVideoAtPathToSavedPhotosAlbum:_capturedFileURL
                                completionBlock:^(NSURL *assetURL, NSError *error) {
//                                    if (error)
//                                        [self showError:error];
//                                    else
//                                        [self removeFile:movieURL];
                                    
                                    dispatch_async(_movieWritingQueue, ^{
                                        _recordingWillBeStopped = NO;
                                        _recording = NO;
                                        
                                        if([self.delegate respondsToSelector:@selector(blCameraControllerDidStopRecordingVideo:)])
                                            [self.delegate blCameraControllerDidStopRecordingVideo:self];
                                    });
                                }];
}

- (void) startVideoRecording
{
    if(!_movieWritingQueue)
        return;
    
    dispatch_async(_movieWritingQueue, ^{
        
        if ( _recordingWillBeStarted || _recording )
            return;
        
        _recordingWillBeStarted = YES;
//        _recording=YES;
        
        // recordingDidStart is called from captureOutput:didOutputSampleBuffer:fromConnection: once the asset writer is setup
        if([self.delegate respondsToSelector:@selector(blCameraControllerWillStartRecordingVideo:)])
            [self.delegate blCameraControllerWillStartRecordingVideo:self];
        
        // Remove the file if one with the same name already exists
//        [self removeFile:movieURL];
        
        // Create an asset writer
        NSError *error;
//        _assetWriter = [[AVAssetWriter alloc] initWithURL:_capturedFileURL fileType:(NSString *)kUTTypeQuickTimeMovie error:&error];
        _assetWriter = [[AVAssetWriter alloc] initWithURL:_capturedFileURL fileType:AVFileTypeMPEG4 error:&error];

//        if (error)
//            [self showError:error];
    });

//    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    
}

- (void) stopVideoRecording
{
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        if ( _recordingWillBeStopped || _recording==NO )
            return;
        
        _recordingWillBeStopped = YES;
        [self stopAndTearDownCaptureSession];
        
        // recordingDidStop is called from saveMovieToCameraRoll
        if([self.delegate respondsToSelector:@selector(blCameraControllerWillStopRecordingVideo:)])
            [self.delegate blCameraControllerWillStopRecordingVideo:self];
        
        if(_assetWriter!=nil)
        {
//            if(_assetWriter.status==AVAssetWriterStatusWriting)
            [_assetWriter finishWritingWithCompletionHandler:^{
                
                _assetWriter = nil;
                //                    [self saveMovieToCameraRoll];
                
            }];
        }
        _readyToRecordVideo = NO;
        _readyToRecordAudio = NO;
        
        _recordingWillBeStopped = NO;
        _recording = NO;
        
        if([self.delegate respondsToSelector:@selector(blCameraControllerDidStopRecordingVideo:)])
        {
            [self.delegate blCameraControllerDidStopRecordingVideo:self];
        }
        else
        {
            OrbisCameraViewController *customCameraController=[OrbisCameraViewController getCameraController];
            if([customCameraController respondsToSelector:@selector(blCameraControllerDidStopRecordingVideo:)])
            {
                [customCameraController blCameraControllerDidStopRecordingVideo:self];
            }
        }

    });
}

- (void) capturePhoto
{
    
}

- (void) writeSampleBuffer:(CMSampleBufferRef)sampleBuffer ofType:(NSString *)mediaType
{
    if ( _assetWriter.status == AVAssetWriterStatusUnknown ) {
        
        if ([_assetWriter startWriting]) {
            [_assetWriter startSessionAtSourceTime:CMSampleBufferGetPresentationTimeStamp(sampleBuffer)];
        }
        else {
//            [self showError:[_assetWriter error]];
        }
    }
    
    if ( _assetWriter.status == AVAssetWriterStatusWriting ) {
        
        if (mediaType == AVMediaTypeVideo) {
            if (_assetWriterVideoIn.readyForMoreMediaData) {
                if (![_assetWriterVideoIn appendSampleBuffer:sampleBuffer]) {
//                    [self showError:[_assetWriter error]];
                }
            }
        }
        else if (mediaType == AVMediaTypeAudio) {
            if (_assetWriterAudioIn.readyForMoreMediaData) {
                if (![_assetWriterAudioIn appendSampleBuffer:sampleBuffer]) {
//                    [self showError:[assetWriter error]];
                }
            }
        }
    }
}

- (BOOL) setupAssetWriterAudioInput:(CMFormatDescriptionRef)currentFormatDescription
{
    const AudioStreamBasicDescription *currentASBD = CMAudioFormatDescriptionGetStreamBasicDescription(currentFormatDescription);
    
    size_t aclSize = 0;
    const AudioChannelLayout *currentChannelLayout = CMAudioFormatDescriptionGetChannelLayout(currentFormatDescription, &aclSize);
    NSData *currentChannelLayoutData = nil;
    
    // AVChannelLayoutKey must be specified, but if we don't know any better give an empty data and let AVAssetWriter decide.
    if ( currentChannelLayout && aclSize > 0 )
        currentChannelLayoutData = [NSData dataWithBytes:currentChannelLayout length:aclSize];
    else
        currentChannelLayoutData = [NSData data];
    
    NSDictionary *audioCompressionSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                              [NSNumber numberWithInteger:kAudioFormatMPEG4AAC], AVFormatIDKey,
                                              [NSNumber numberWithFloat:currentASBD->mSampleRate], AVSampleRateKey,
                                              [NSNumber numberWithInt:64000], AVEncoderBitRatePerChannelKey,
                                              [NSNumber numberWithInteger:currentASBD->mChannelsPerFrame], AVNumberOfChannelsKey,
                                              currentChannelLayoutData, AVChannelLayoutKey,
                                              nil];
    if ([_assetWriter canApplyOutputSettings:audioCompressionSettings forMediaType:AVMediaTypeAudio]) {
        _assetWriterAudioIn = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeAudio outputSettings:audioCompressionSettings];
        _assetWriterAudioIn.expectsMediaDataInRealTime = YES;
        if ([_assetWriter canAddInput:_assetWriterAudioIn])
            [_assetWriter addInput:_assetWriterAudioIn];
        else {
            NSLog(@"Couldn't add asset writer audio input.");
            return NO;
        }
    }
    else {
        NSLog(@"Couldn't apply audio output settings.");
        return NO;
    }
    
    return YES;
}



- (BOOL) setupAssetWriterVideoInput:(CMFormatDescriptionRef)currentFormatDescription
{
    CMVideoDimensions dimensions = CMVideoFormatDescriptionGetDimensions(currentFormatDescription);
    NSDictionary *videoCompressionSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                              AVVideoCodecH264, AVVideoCodecKey,
                                                [NSNumber numberWithInteger:dimensions.width], AVVideoWidthKey,
                                                [NSNumber numberWithInteger:dimensions.height], AVVideoHeightKey,
                                                [NSDictionary dictionaryWithObjectsAndKeys:
                                                [NSNumber numberWithInteger:_capturingBitRate], AVVideoAverageBitRateKey,
#pragma mark -  Added BaseLine Profile
//                                               AVVideoProfileLevelH264Baseline30,AVVideoProfileLevelKey,
                                               
                                               [NSNumber numberWithInteger:30], AVVideoMaxKeyFrameIntervalKey,
                                               nil], AVVideoCompressionPropertiesKey,
                                              nil];
    if ([_assetWriter canApplyOutputSettings:videoCompressionSettings forMediaType:AVMediaTypeVideo]) {
        _assetWriterVideoIn = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeVideo outputSettings:videoCompressionSettings];
        _assetWriterVideoIn.expectsMediaDataInRealTime = YES;
//        _assetWriterVideoIn.transform = [self transformFromCurrentVideoOrientationToOrientation:_referenceOrientation];
        if ([_assetWriter canAddInput:_assetWriterVideoIn])
            [_assetWriter addInput:_assetWriterVideoIn];
        else {
            NSLog(@"Couldn't add asset writer video input.");
            return NO;
        }
    }
    else {
        NSLog(@"Couldn't apply video output settings.");
        return NO;
    }
    
    return YES;
}


- (BOOL) setupCaptureSession
{
    _captureSession = [[AVCaptureSession alloc] init];
    
    _captureAudioInput = [[AVCaptureDeviceInput alloc] initWithDevice:[self getAudioCaptureDevice] error:nil];
    if ([_captureSession canAddInput:_captureAudioInput])
        [_captureSession addInput:_captureAudioInput];
    
    AVCaptureAudioDataOutput *audioOut = [[AVCaptureAudioDataOutput alloc] init];
    dispatch_queue_t audioCaptureQueue = dispatch_queue_create("Audio Capture Queue", DISPATCH_QUEUE_SERIAL);
    [audioOut setSampleBufferDelegate:self queue:audioCaptureQueue];

    if ([_captureSession canAddOutput:audioOut])
        [_captureSession addOutput:audioOut];
    _audioConnection = [audioOut connectionWithMediaType:AVMediaTypeAudio];
    
    [self setCaptureDevicePosition:_captureDevicePosition];
    _captureDevice=[self getVideoCaptureDeviceWithPosition:_captureDevicePosition];
    if(_captureVideoInput)
        [_captureSession removeInput:_captureVideoInput];
    _captureVideoInput = [[AVCaptureDeviceInput alloc] initWithDevice:_captureDevice error:nil];
    if ([_captureSession canAddInput:_captureVideoInput])
        [_captureSession addInput:_captureVideoInput];
    
    if(_captureVideoOutput)
        [_captureSession removeOutput:_captureVideoOutput];
    _captureVideoOutput = [[AVCaptureVideoDataOutput alloc] init];
    [_captureVideoOutput setAlwaysDiscardsLateVideoFrames:NO];
    NSMutableDictionary *videosettings=[NSMutableDictionary dictionary];
//    NSMutableDictionary * compressionSettings = [[NSMutableDictionary alloc] initWithCapacity:1];
//    [compressionSettings setObject:[NSNumber numberWithInt:512000] forKey:AVVideoAverageBitRateKey];
//    [videosettings setObject:compressionSettings forKey:AVVideoCompressionPropertiesKey];
    [videosettings setObject:[NSNumber numberWithInt:kCVPixelFormatType_32BGRA] forKey:(id)kCVPixelBufferPixelFormatTypeKey];
    [_captureVideoOutput setVideoSettings:videosettings];
    
    dispatch_queue_t videoCaptureQueue = dispatch_queue_create("Video Capture Queue", DISPATCH_QUEUE_SERIAL);
    [_captureVideoOutput setSampleBufferDelegate:self queue:videoCaptureQueue];
    
    if ([_captureSession canAddOutput:_captureVideoOutput])
        [_captureSession addOutput:_captureVideoOutput];
    
    AVCaptureVideoOrientation videoOrientation=[self getCurrentOrientation];
    _videoConnection = [_captureVideoOutput connectionWithMediaType:AVMediaTypeVideo];
    [_videoConnection setVideoOrientation:(videoOrientation==AVCaptureVideoOrientationLandscapeRight || videoOrientation==AVCaptureVideoOrientationLandscapeLeft)?videoOrientation:AVCaptureVideoOrientationPortrait];
    _videoOrientation = [_videoConnection videoOrientation];
    
    if([_captureSession canSetSessionPreset:_captureSessionPreset])
        _captureSession.sessionPreset = _captureSessionPreset;
    else
    {
//        if([_captureSessionPreset isEqualToString:AVCaptureSessionPreset1920x1080]) {
//            [self setCaptureSessionPreset:AVCaptureSessionPresetHigh];
//        }
        if([_captureSessionPreset isEqualToString:AVCaptureSessionPreset1280x720]) {
            [self setCaptureSessionPreset:AVCaptureSessionPresetHigh];
        }
        else
        {
            [self setCaptureSessionPreset:AVCaptureSessionPresetMedium];
        }
        
        if([_captureSession canSetSessionPreset:_captureSessionPreset])
            _captureSession.sessionPreset = _captureSessionPreset;
    }
    
 
    if (!_overlayView) {
        _overlayView=[[UIView alloc] initWithFrame:self.view.bounds];
       
    }
    
    if(_previewLayer)
    {
        [_previewLayer removeFromSuperlayer];
        _previewLayer=nil;
    }
    
    _previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    CGRect layerRect = [[[self view] layer] bounds];
    
    if ([_previewLayer respondsToSelector:@selector(connection)])
    {
        if ([_previewLayer.connection isVideoOrientationSupported])
        {
            [_previewLayer.connection setVideoOrientation:videoOrientation];
        }
    }
    
//    UIInterfaceOrientation interfaceOrientation = self.interfaceOrientation;
//    [self changeOrientation:interfaceOrientation];
    
    [_previewLayer setFrame:layerRect];
    _previewLayer.videoGravity=AVLayerVideoGravityResizeAspectFill;
    //    _previewLayer.contentsGravity=kCAGravityResizeAspectFill;
    [_previewLayer setPosition:CGPointMake(CGRectGetMidX(layerRect), CGRectGetMidY(layerRect))];
    
    [self.view.layer addSublayer:_previewLayer];
    [self.view addSubview:_overlayView];
    
    
   [self performSelector:@selector(addLayerView) withObject:nil afterDelay:0.5];
   // [self addLayerView];
    return YES;
}

-(void)addLayerView
{
    [_overlayView setFrame:self.view.bounds];

}



- (void) setupAndStartCaptureSession
{
    _movieWritingQueue = dispatch_queue_create("Movie Writing Queue", DISPATCH_QUEUE_SERIAL);
    
    if ( !_captureSession )
        [self setupCaptureSession];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(captureSessionStoppedRunningNotification:) name:AVCaptureSessionDidStopRunningNotification object:_captureSession];
    
    if ( !_captureSession.isRunning )
        [_captureSession startRunning];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationDidChangeNotificationReceived:) name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void) orientationDidChangeNotificationReceived:(NSNotification*)notification
{
    [self changeVideoConnectionOrientation];
}

- (void) changeVideoConnectionOrientation
{
    AVCaptureVideoOrientation videoOrientation=[self getCurrentOrientation];
//    if ([_videoConnection isVideoOrientationSupported]) {
//        _videoConnection.videoOrientation=videoOrientation;
//        _videoOrientation = videoOrientation;
        _referenceOrientation=videoOrientation;
//    }
    
    if(_captureSession)
    {
            if ([_previewLayer respondsToSelector:@selector(connection)])
            {
                if ([_previewLayer.connection isVideoOrientationSupported])
                {
                    [_previewLayer.connection setVideoOrientation:_referenceOrientation];
                    
                    CGRect layerRect = [[[self view] layer] bounds];
                    [_previewLayer setFrame:layerRect];
                    _previewLayer.videoGravity=AVLayerVideoGravityResizeAspectFill;
                }
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [_captureSession beginConfiguration];
                [_videoConnection setVideoOrientation:videoOrientation];
                [_captureSession commitConfiguration];
            });
        }
        
    }

}

- (void) pauseCaptureSession
{
    if ( _captureSession.isRunning )
        [_captureSession stopRunning];
}

- (void) resumeCaptureSession
{
    if ( !_captureSession.isRunning )
        [_captureSession startRunning];
}

- (void)captureSessionStoppedRunningNotification:(NSNotification *)notification
{
    dispatch_async(_movieWritingQueue, ^{
        if (_recording) {
            [self stopVideoRecording];
        }
    });
}

- (void) stopAndTearDownCaptureSession
{
    if(_captureSession && [_captureSession isRunning])
        [_captureSession stopRunning];
//    if (_captureSession)
//        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureSessionDidStopRunningNotification object:_captureSession];
    _captureSession = nil;
    if (_movieWritingQueue) {
        _movieWritingQueue = NULL;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}



#pragma mark - AVCaptureAudioDataOutputSampleBufferDelegate Methods

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection
{
    CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer);
    
    if ( connection == _videoConnection ) {
        // Get buffer type
        if ( _videoType == 0 )
            _videoType = CMFormatDescriptionGetMediaSubType( formatDescription );
    }
    
    CFRetain(sampleBuffer);
    CFRetain(formatDescription);
    dispatch_async(_movieWritingQueue, ^{
        
        if ( _assetWriter ) {
            
            BOOL wasReadyToRecord = (_readyToRecordAudio && _readyToRecordVideo);
            
//            NSLog(@"inputPorts %@", connection.inputPorts);
            
            AVCaptureInputPort *inputPort=connection.inputPorts.firstObject;
            
            if (connection == _videoConnection || [inputPort.mediaType isEqualToString:AVMediaTypeVideo]) {
                
                _videoConnection = connection;
                // Initialize the video input if this is not done yet
                if (!_readyToRecordVideo)
                    _readyToRecordVideo = [self setupAssetWriterVideoInput:formatDescription];
                
                // Write video data to file
                if (_readyToRecordVideo && _readyToRecordAudio)
                {
                    [self writeSampleBuffer:sampleBuffer ofType:AVMediaTypeVideo];
                    
                    if(delegate && [delegate respondsToSelector:@selector(blCameraControllerDidCaptureSampleBuffer:)])
                    {
                        CMSampleBufferRef bufferCopy=NULL;
                        CMSampleBufferCreateCopy(kCFAllocatorDefault, sampleBuffer, &bufferCopy);
                        [delegate blCameraControllerDidCaptureSampleBuffer:bufferCopy];
                    }
                }
            }
            else if (connection == _audioConnection || [inputPort.mediaType isEqualToString:AVMediaTypeAudio]) {
                
                // Initialize the audio input if this is not done yet
                if (!_readyToRecordAudio)
                    _readyToRecordAudio = [self setupAssetWriterAudioInput:formatDescription];
                
                // Write audio data to file
                if (_readyToRecordAudio && _readyToRecordVideo)
                    [self writeSampleBuffer:sampleBuffer ofType:AVMediaTypeAudio];
            }
            
            BOOL isReadyToRecord = (_readyToRecordAudio && _readyToRecordVideo);
            if ( !wasReadyToRecord && isReadyToRecord ) {
                _recordingWillBeStarted = NO;
                _recording = YES;
                if([self.delegate respondsToSelector:@selector(blCameraControllerDidStartRecordingVideo:)])
                    [self.delegate blCameraControllerDidStartRecordingVideo:self];
            }
        }
        CFRelease(sampleBuffer);
        CFRelease(formatDescription);
    });
}







@end
