//
//  ImagePreview.swift
//  GLF. Locker
//
//  Created by Jawad Ahmed on 21/02/2017.
//  Copyright © 2017 Nasir Mehmood. All rights reserved.
//

import UIKit

class ImagePreview: UIView {

    @IBOutlet var mainImageView: UIImageView!
    var imagePath: String?
    
    
   @objc static func loadFromXib() -> ImagePreview {
        return Bundle.main.loadNibNamed("ImagePreview", owner: self, options: nil)?.first as! ImagePreview
    }
    
    @objc func addInView(parentView: UIView) {
      
        self.frame = parentView.frame
        parentView.addSubview(self)
        if (imagePath != nil) {
            var fileUrl = imagePath!.replacingOccurrences(of: "~", with: "", options: .regularExpression)
            fileUrl = baseUrlForVideo + fileUrl
            mainImageView.sd_setImage(with: URL.init(string: fileUrl), completed: nil)
            layoutSubviews()
        }
    }
    
    
   /* func decidingImageFrame()  {
        
    
        let maxHeight = (self.window?.screen.applicationFrame.size.height)! - 45;
        
//        CGFloat width = (image.size.width * maxHeight) / image.size.height;
//        self.imageViewWidthConstraint.constant = width;//size.width ;
        
        self.frame = CGRect(x: 0, y: 0, width: <#T##CGFloat#>, height: <#T##CGFloat#>)

    }*/
    
    
    @IBAction func removeAction() {
        self.removeFromSuperview()
    }
}
