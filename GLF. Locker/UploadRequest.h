//
//  UploadRequest.h
//  sibme
//
//  Created by Nasir Mehmood on 3/21/13.
//
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <AVFoundation/AVFoundation.h>
#import "OrbisConstants.h"

#define kLessonTypeOneToOne 1
#define kLessonTypeClass 2
#define kLessonTypeMedia 1000
#define kChunkSize 200*1024


//old urls live

//#define kMediaUploadingURL @"https://app.glflocker.com/OrbisAppBeta/api/Coach/UploadLessonMediaFile"
//#define kAddToLessonURL @"https://app.glflocker.com/OrbisAppBeta/api/Coach/AddLessonMedia"
//#define kVideoStatusURL @"https://app.glflocker.com/OrbisAppBeta/Api/Academy/getmediastatusbyid/"

//Live


#define kMediaUploadingURL @"https://app.glflocker.com/OrbisWebApi/api/Coach/UploadLessonMediaFile"
#define kAddToLessonURL @"https://app.glflocker.com/OrbisWebApi/api/Coach/AddLessonMedia"
#define kVideoStatusURL @"https://app.glflocker.com/OrbisWebApi/Api/Academy/getmediastatusbyid/"


//test Flight

//#define kMediaUploadingURL @"https://app.glflocker.com/webserviceapp/api/Coach/UploadLessonMediaFile"
//#define kAddToLessonURL @"https://app.glflocker.com/webserviceapp/api/Coach/AddLessonMedia"
//#define kVideoStatusURL @"https://app.glflocker.com/webserviceapp/Api/Academy/getmediastatusbyid/"

//Beta
//#define kMediaUploadingURL @"http://glfbeta.com/OrbisWebApi/api/Coach/UploadLessonMediaFile"
//#define kAddToLessonURL @"http://glfbeta.com/OrbisWebApi/api/Coach/AddLessonMedia"
//#define kVideoStatusURL @"http://glfbeta.com/OrbisWebApi/Api/Academy/getmediastatusbyid/"


//#define kVideoStatusURL @"https://app.glflocker.com/OrbisAppBeta/Api/Academy/getmediastatusbyid/" // Live

typedef enum : NSUInteger {
    kMediaTypeVideo=1,
    kMediaTypePhoto,
    kMediaTypeDrill,
    kMediaTypeOverlay
} OrbisMediaType;


@class UploadRequest;

@protocol UploadRequestDelgate <NSObject>

- (void) uploadRequestCompleted:(UploadRequest*)uploadRequest;
- (void) uploadRequestFailed:(UploadRequest*)uploadRequest;
- (void) uploadRequestStopped:(UploadRequest*)uploadRequest;

@end

@interface UploadRequest : NSObject{
    
}

@property (nonatomic) BOOL isUploading;
@property (nonatomic) BOOL shouldUpload;
@property (nonatomic) NSUInteger chunkSize;

@property (nonatomic, strong) NSMutableDictionary *requestOptions;

@property (nonatomic, weak) id<UploadRequestDelgate> uploadRequestDelegate;

- (id) initWithOptions:(NSDictionary*) options uploadRequestDelegate:(id<UploadRequestDelgate>) delegate;

- (void) startRequest;
- (void) cancelRequest;
- (NSString*)uniqueMediaID;
- (void)uploadingCompletedWithResponse:(NSHTTPURLResponse *)response responseData:(NSData*)responseData mediaInfo:(NSDictionary*)mediaInfo;
- (void) updatePogressWithMediaInfo:(NSDictionary*)mediaInfo didSendData:(long long)bytesWritten totalBytesWritten:(long long)totalBytesWritten totalBytesExpectedToWrite:(long long)totalBytesExpectedToWrite;
- (void) uploadingFailedWithError:(NSError*)error mediaInfo:(NSDictionary*)mediaInfo;

- (void) startUploading;
- (void) stopUploading;
- (void) resumeUploading;
- (void) cancelUploading;
+(void)uploadVideoDimentions:(int )mediaId andWidth:(int )width andHeight:(int  )height;

- (UploadRequestStatus) requestStatus;
- (void) setRequestStatus:(UploadRequestStatus)status;

@end



