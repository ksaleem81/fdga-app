//
//  UIImage+fixOrientation.h
//  Orbis
//
//  Created by MacBook Pro on 16/07/2015.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;
@end
