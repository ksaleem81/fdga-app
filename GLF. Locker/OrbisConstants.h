//
//  SibmeConstants.h
//  sibme2.0
//
//  Created by Nasir Mehmood on 5/30/15.
//
//

#ifndef sibme2_0_OrbisConstants_h
#define sibme2_0_OrbisConstants_h

#pragma mark - Upload Request Constants



//old url live
//static NSString * const kMediaUploadURL = @"https://app.glflocker.com/OrbisAppBeta/api/Coach/MyPostFile";
//static NSString * const kVideoDimentionsUploadURL=@"https://app.glflocker.com/OrbisAppBeta/api/Academy/UpdateVideoDimensions";


//Live

static NSString * const kMediaUploadURL = @"https://app.glflocker.com/OrbisWebApi/api/Coach/MyPostFile";
static NSString * const kVideoDimentionsUploadURL=@"https://app.glflocker.com/OrbisWebApi/api/Academy/UpdateVideoDimensions";
static NSString * const baseUrlForVideo = @"https://app.glflocker.com";

//test Flight
//
//static NSString * const kMediaUploadURL = @"https://app.glflocker.com/webserviceapp/api/Coach/MyPostFile";
//static NSString * const kVideoDimentionsUploadURL=@"https://app.glflocker.com/webserviceapp/api/Academy/UpdateVideoDimensions";
//static NSString * const baseUrlForVideo = @"https://app.glflocker.com";

//Beta

//static NSString * const kMediaUploadURL = @"http://app.glfbeta.com/OrbisWebApi/api/Coach/MyPostFile";
//static NSString * const kVideoDimentionsUploadURL=@"http://app.glfbeta.com/OrbisWebApi/api/Academy/UpdateVideoDimensions";
//static NSString * const baseUrlForVideo = @"http://app.glfbeta.com";

static NSString * const kResponseFileNameKey=@"orbisFileName";
static NSString * const kFileNameKey=@"orbisFileName";
static NSString * const kMethodNameKey=@"orbisMethodName";
static NSString * const kMediaIDKey=@"uniqueMediaID";



static NSString * const kMethodNameCreateAndSignUploadpart=@"createAndSignUploadpart";
static NSString * const kMethodNameUploadChunk=@"uploadChunk";
static NSString * const kMethodNameUploadThumbnail=@"uploadThumbnail";
static NSString * const kMethodNameCompleteAndPublishMultipart=@"completeAndPublishMultipart";

static NSString * const kChunkPriorityKey=@"chunkPriorityKey";
static NSString * const kChunkNumberKey=@"chunkNumberKey";
static NSString * const kChunkStatusKey=@"chunkStatusKey";
static NSString * const kChunkSizeKey=@"chunkSizeKey";
static NSString * const kChunkOffsetKey=@"chunkOffsetKey";

typedef enum: NSUInteger{
    ChunkUploadStatusNotInitial = 1000,
    ChunkUploadStatusInitial,
    ChunkUploadStatusHaveUploadInfo,
    ChunkUploadStatusInQueue,
    ChunkUploadStatusUploaded,
    ChunkUploadStatusCancelled,
    ChunkUploadStatusWaiting
} ChunkUploadStatus;


static NSString * const kUploadRequestStatusKey=@"uploadRequestStatusKey";
typedef enum : NSUInteger {
    UploadRequestStatusNotStarted=1110,
    UploadRequestStatusStarted,
    UploadRequestStatusSuspended,
    UploadRequestStatusSigningParts,
    UploadRequestStatusUploadingParts,
    UploadRequestStatusUploadingThumbnail,
    UploadRequestStatusThumbnailUploaded,
    UploadRequestStatusPublished,
    UploadRequestStatusPublishing,
    UploadRequestStatusPaused,
    UploadRequestStatusUploaded
} UploadRequestStatus;


#pragma mark -


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

static BOOL const kUseOldUploadingMethod = NO;  //  YES/NO

#define kOrbisVideoQualityKey @"OrbisVideoQuality"


static NSString * const kEventTypeKey = @"EventType";
static NSString * const kEventTypeLeaveKey = @"Leave";
static NSString * const kEventProgramTypeIDKey = @"PorgramTypeId";
static int kEventProgramTypeIDFullDayLeave = 1;
static NSString * const kiOSCalendarName = @"GLF. Locker Calendar";
//static NSString * const kiOSCalendarName = @"Jay Kelly Golf Calendar";



#define kResumeVideoUploadingNotification       @"ResumeVideoUploadingNotification"
#define kUploadingVideoFailedNotifiation        @"UploadVideoFailedToStart"




#endif
