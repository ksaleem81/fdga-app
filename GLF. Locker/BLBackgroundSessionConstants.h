//
//  TNBackgroundSessionConstants.h
//  Telenotes2
//
//  Created by Stephen Lynn on 3/12/15.
//  Copyright (c) 2015 Nasir Mehmood. All rights reserved.
//

#ifndef Orbis_TNBackgroundSessionConstants____FILEEXTENSION___
#define Orbis_TNBackgroundSessionConstants____FILEEXTENSION___

static NSString * const kBackgroundUploadIdentifier = @"kOrbisBackgroundUploadIdentifier";
static NSString * const kBackgroundUploadSessionsArrayKey = @"kBackgroundUploadSessionsArrayKey";
static NSString * const kBackgroundUploadIdentifiersArrayKey = @"kBackgroundUploadIdentifiersArray";




#endif
