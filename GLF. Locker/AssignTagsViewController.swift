//
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit
import GoogleMaps
import SDWebImage
import SideMenu

protocol SelectedSkillsDelegateForTags {
    func seletedSkillsData(selectedData: [[String:AnyObject]],selectedUserId:String)
}

class AssignTagsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{

  
    var delegate : SelectedSkillsDelegateForTags?
    
    var selectedItems = [[String:AnyObject]]()
    var dataArray = [[String:AnyObject]]()
    var selectedDataArray = [[String:AnyObject]]()
    var previousData = [String:AnyObject]()


    @IBOutlet weak var tableView: UITableView!
    
 

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.

        ////GetAllAcademiesOrderByLatLong
        settingRighMenuBtn()
            tableView.separatorColor = UIColor.init(red: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)

    }


    func settingRighMenuBtn()  {
        let button = UIButton.init(type: .custom)
        button.setImage(UIImage.init(named: "lines_3"), for: UIControlState.normal)
        button.addTarget(self, action:#selector(AssignTagsViewController.rightBarBtnAction), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30) //CGRectMake(0, 0, 30, 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    @objc func rightBarBtnAction() {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
  
    }
    
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
 
    }

    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
 
        self.tableView.reloadData()
     
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 //MARK:- tableview dataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssignTagsTableViewCell", for: indexPath) as! AssignTagsTableViewCell
        if let title = dataArray[indexPath.row]["SkillName"] as? String{
           cell.companyLbl.text = title
        }
        var skilId = ""
        if let title = dataArray[indexPath.row]["SkillID"] as? Int{
            skilId = "\(title)"
        }
        
        //check to check and uncheck radio btn
        let k = self.selectedItems
        let index = k.index {
            if let dic = $0 as? Dictionary<String,AnyObject> {
                if let value = dic["SkillID"]  as? Int, "\(value)" == "\(skilId)" {
                    cell.radioBtn.isSelected = true
                    return true
                }
            }
            cell.radioBtn.isSelected = false
            return false
        }
        
        if index == nil {
            cell.radioBtn.isSelected = false
        }
        
        cell.radioBtn.tag = indexPath.row
        cell.radioBtn.addTarget(self, action:#selector(selectCheckBtn(sender:)) , for:.touchUpInside)
        
        if let type = previousData["ProgramType"] as? String {
            
            if type == "2" {
                cell.radioBtn.isUserInteractionEnabled = false

            }
        }
        
               return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
    
    }
    
    @objc func selectCheckBtn(sender:UIButton)  {
        
        let indexPath =  IndexPath(row: sender.tag, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! AssignTagsTableViewCell
        
        var skilId = ""
        if let title = dataArray[indexPath.row]["SkillID"] as? Int{
            skilId = "\(title)"
        }
        
        if cell.radioBtn.isSelected {
            cell.radioBtn.isSelected = false
          //  self.selectedDataArray.
            let k = self.selectedItems
            let index = k.index {
                if let dic = $0 as? Dictionary<String,AnyObject> {
                    if let value = dic["SkillID"]  as? Int, "\(value)" == skilId{
                        return true
                    }
                }
                return false
            }
            
//            if index == nil {
//                let index1 = k.index {
//                    if let dic = $0 as? Dictionary<String,AnyObject> {
//                        if let value = dic["SkillID"]  as? Int, "\(value)" == skilId{
//                            return true
//                        }
//                    }
//                    return false
//                }
//                self.selectedItems.remove(at: index1!)
//
//                
//            }else{
                self.selectedItems.remove(at: index!)
          //  }
            
             self.delegate?.seletedSkillsData(selectedData: selectedItems,selectedUserId:"")
        }else{
            
            cell.radioBtn.isSelected = true
            selectedItems.append(dataArray[indexPath.row])
             self.delegate?.seletedSkillsData(selectedData: selectedItems,selectedUserId:"")
           // getUserInfo(tag: cell.radioBtn.tag)
        }
    }
    

   

}

