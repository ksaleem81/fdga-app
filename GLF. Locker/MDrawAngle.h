//
//  MDrawAngle.h
//  Motics
//
//  Created by Gao Yongqing on 2/6/14.
//
//

#import "MDrawPolyline.h"
//#import "AppDelegate.h"
#import "GlobalClass.h"

@interface MDrawAngle : MDrawPolyline

@property(nonatomic,retain) GlobalClass *delObj;
@property int degree ;
@end
