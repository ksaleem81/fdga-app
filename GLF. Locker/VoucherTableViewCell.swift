//
//  MapTableViewCell.swift
//  GLF. Locker
//
//  Created by Muhammad Arslan Khalid on 12/27/16.
//  Copyright © 2016 Nasir Mehmood. All rights reserved.
//

import UIKit

class VoucherTableViewCell: UITableViewCell {

    @IBOutlet weak var voucherPin: UILabel!
    @IBOutlet weak var radioBtn: UIButton!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var detailHeadLbl: UILabel!
    @IBOutlet weak var valueHeadLbl: UILabel!
    @IBOutlet weak var detailLbl: UILabel!
    @IBOutlet weak var voucherHeadLbl: UILabel!
    @IBOutlet weak var lessonTypLbl: UILabel!
    @IBOutlet weak var remainingLbl: UILabel!
    @IBOutlet weak var remainingValueLbl: UILabel!
    @IBOutlet weak var lessonValueLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
